<?php
/*!******************************************************************
fichier: gestLib.php
auteur : Pascal TOLEDO
date de creation: 01 fevrier 2012
date de modification: 08 juin 2014
source: http://gitorious.org/gestLibs
depend de:
	* aucune
description:
	* class de verification de librairie
	* fonction de debug avec niveau d'erreur
	* duree de parsing du fichier
documentation:
	* http://php.net/manual/fr/language.constants.predefined.php
*******************************************************************/
define ('GESTLIBVERSION','1.0.0');
//********************************************
// CLASS DE GESTION DES ERREURS
//********************************************

//if (gettype($gestLib)=='NULL')	// protection gestLib
//{
class LEGRALERR
	{
	const ALWAYS=-1;//sera toujours afficher quelque soit le niveau atrtribuer pour la lib

	const NOERROR=0;
	const CRITIQUE=1;
	const DEBUG=2;
	const WARNING=3;
	const INFO=4;
	const ALL=5;
	}
function LEGRALERR_toString($err)
	{
	switch ($err)
		{
		case LEGRALERR::NOERROR :return 'NOERROR';
		case LEGRALERR::CRITIQUE :return 'CRITIQUE';
		case LEGRALERR::DEBUG :return 'DEBUG';
		case LEGRALERR::WARNING :return 'WARNING';
		case LEGRALERR::INFO :return 'INFO';
		case LEGRALERR::ALWAYS :return 'ALWAYS';
		}
	}

//********************************************
// CLASS DE GESTION DES ETATS
//********************************************
class LEGRAL_LIBETAT
	{
	const NOLOADED=0;
	const LOADING=1;
	const LOADED=2;
	}
function LEGRAL_LIBETAT_toString($etat)
	{
	switch ($etat)
		{
		case LEGRAL_LIBETAT::NOLOADED :return 'NOLOADED';
		case LEGRAL_LIBETAT::LOADING :return 'LOADING';
		case LEGRAL_LIBETAT::LOADED :return 'LOADED';
		}
	}
//}// finc protection gestLib

//********************************************
// function gestLib_inspect() 
// - manipuler des variables independantes des libs - //
// - retourne les donnees d'une variable independante (nom,$var) au couleur de gestLib.css - //
// - si $varNom='' alors valeur seul - //
// - line,method: voir les info de debuggage - //
//********************************************
function gestLib_inspect($varNom,$var,$line=0,$methode='')
        {
        $out='';

        if($varNom!='')$out.='<span class="legralLibVar">'.$varNom.'</span>='; // pas de nom preciser

        if (!empty($line)){$out.="(<span class='legralLibInfo'>$line</span>)";}
        if (!empty($methode)){$out.="[<span class='legralLibInfo'>$methode</span>]";}

	$out.='<span class="legralLibVal">';
//	if ($var===NULL){$out.='<i>NULL</i></span><br>';return $out;}
        if ($var===0)   {$out.='<i>0</i></span><br>';return $out;}
        if ($var==='')  {$out.="'vide'</span><br>";return $out;}
	if (empty($var)){$out.='<i>empty</i></span><br>';return $out;}
	
	switch(gettype($var)){
		case'boolean': case'interger': case'double':
			$out.="num:$var</span><br>";return $out;break;
		case'string':
			$out.="'$var'</span><br>";return $out;break;
		case'NULL':
			$out.='<i>NULL</i></span><br>';return $out;break;
		case'ressource':
			$out.='(ressource)</span><br>';return $out;break;
		case'array':
			$nb=count($var);
			if($nb>0){
				$out.="(array:$nb)<ul class='gestLibArray'>";
				foreach($var as $key => $value)$out.='<li style="list-style-type:decimal">'.gestLib_inspect($key,$value).'</li>';
				}
			return "$out</ul></span>";
                        break;

		case'object':
			$out.='(object:'.count($var).')<ol class="gestLibObject">';
			//if(get_class_vars($var))$out.='.';	// renvoie les valeurs par defaut
			if(!get_class_methods($var))$out.='<i>Classe sans methode.</i>';

			foreach($var as $key => $value)$out.='<li style="list-style-type:upper-roman">'.gestLib_inspect($key,$value).'</li>';
			return "$out</ol></span>";

			break;
		case'unknoww type':
			$out.='<i>unknoww type</i></span><br>';return $out;break;	
		default:
			$out.="(default):$var</span><br>";return $out;break;
		}
        }



//********************************************
// CLASS DE DONNEES
//********************************************
class gestLib_legralLib
{
	public $nom=NULL;
	public $fichier=NULL;
	public $version=NULL;

	public $auteur='fenwe';
	public $site='http://legral.fr';
	public $git='http://gitorious.org/~fenwe';
	public $description='';


	public $etat=NULL;	//1: en cours de chargement;2: chargement terminer

	function setEtat($etat){$this->etat=$etat;}
	var $dur=0;
        var $deb=0;
        var $fin=0;
	
	function getDuree(){return number_format($this->dur,6);}



function end()
	{
	$this->etat=LEGRAL_LIBETAT::LOADED;
	$this->fin=microtime(1);$this->dur=$this->fin - $this->deb;
	}


	public $err_level=0;
	public $err_level_Backup=NULL;	//sauvegarde de l'etat
	function setErr($err){$this->err_level=$err;}
	function setErrLevelTemp($err){$this->err_level_Backup=$this->err_level;$this->err_level=$err_level;}
	function setErrLevelTemp_NOERROR(){$this->err_level_Backup=$this->err_level;$this->err_level=LEGRALERR::NOERROR;}
	function restoreErrLevel(){$this->err_level=$this->err_level_Backup;}


function __construct($nom,$file,$version,$description=NULL)
	{
	$this->deb=microtime(1);
	$this->err_level=LEGRALERR::NOERROR;
	$this->nom=$nom;
	$this->fichier=$file;
	$this->version=$version;
	$this->etat=1;
	$this->description=$description;
	}
function __toString() {return '';}

// - renvoie le texte si le niveau d'erreur concorde - //
function debugShow($level,$line,$methode,$txt,$br=null)
	{
	if($level>$this->err_level){return NULL;}
	$out='<span class="legralLibVar">'.$this->nom;
	$out.='{'.LEGRALERR_toString($level).'}';
	if (!empty($line)){$out.="($line)";}
	if (!empty($methode)){$out.="[$methode]";}
	$out.=':</span>';
	$out.='<span class="legralLibVal">'."$txt</span>";
	if (empty($br)){$out.='<br />';}
	return $out;
	}

// - renvoie le nom et la lvaleur de la variable si le niveau d'erreur concorde - //
function debugShowVar($level,$line,$methode,$varNom,$var,$br1='br')
	{global $gestlib;
	if ($level>$this->err_level){return '';}
	$out='<span class="legralLibNom">'.$this->nom; // nom de la lib
	$out.='{'.LEGRALERR_toString($level).'}';
	if (!empty($line)){$out.="($line)";}
	if (!empty($methode)){$out.="[$methode]";}
	$out.=':</span>';
	$out.='<span class="legralLibVar">'.$varNom.'</span>=';// nom de la variable

	$out.=gestLib_inspect('',$var);	// appel de l'instance predefini

/*
	switch ($br1)
		{
		case null:case 'nobr':$out.='<span class="legralLib_text">'.$val.'</span>';break;
		case 'br':case 1:	  $out.='<span class="legralLib_text">'.$val.'</span><br>';break;
		case 'div':		  $out.='<div class="legralLib_text">'.$val.'</div>';break;
		case 'p':			  $out.='<p class="legralLib_text">'.$val.'</p>';break;
		}
*/
	return $out;
	}

}

//********************************************
// CLASS GESTIONLIBRAIRIE
//********************************************
class gestionLibrairies
{
public $lib=array();	//tableau de lib
//function __construct()	{	}

function __toString(){
	$out='<ul class="gestlib">';
	foreach($this->lib  as $key => $value)  $out.="<li>$key= $value</li>";$out.="</ul></li>";
	$out.='</ul>';
	return $out;
	}
function loadLib($nom,$file,$version,$description=NULL)
	{
//	if ( isset($this->lib[$nom]->nom) ){$this->lib[$nom]->nom=NULL;}//si deja charge
	$this->lib["$nom"]=new gestLib_legralLib($nom,$file,$version,$description);
	}
function tableau()
	{
	$out ='<table class="gestLib"><caption>Librairies PHP</caption>';
	$out.='<thead><tr><th>nom</th><th>version</th><th>etat</th><th>err level</th><th>durée</th><th>description</th><th>auteur</th> <th>site</th> <th>git</th> </tr></thead>';
	foreach($this->lib as $key => $value)//$value= gestLib[index]
		{
//		$libNom=$value->nom;
		$out.='<tr>';
		$out.='<td>'.$value->nom.'</td>';
		$out.='<td>'.$value->version.'</td>';
		$out.='<td>'.LEGRAL_LIBETAT_toString($value->etat).'</td>';
		$out.='<td>'.LEGRALERR_toString($value->err_level).'</td>';
		$out.='<td>'.$value->getDuree().'</td>';
		$out.='<td>'.$value->description.'</td>';
//		$out.='<td>'.$value->auteur.'</td>';
		$t=$value->auteur;	$out.="<td><a target='exterieur' href='$value->site'>$t</a></td>";
		$t=$value->git;		$out.="<td><a target='git' href='$t'>$t</a></td>";
		$out.="</tr>\n";
		};

	$out.='</table>';
	return $out;
	}
function libTableau(){return $this->tableau();}

function setEtat($lib,$etat){$this->lib[$lib]->setEtat($etat);}
function getDuree($lib)     {$this->lib[$lib]->getDuree();}
function end($lib)          {$this->lib[$lib]->end();}
function getLibError($lib)  {if(isset($this->lib[$lib]))return LEGRALERR_toString($this->lib[$lib]->err_level);}

function debugShow($lib,$level,$line='',$methode='',$varNom,$br=NULL){
	if(isset($this->lib[$lib]))return $this->lib[$lib]->debugShow($level,$line,$methode,$varNom,$br=NULL);
	}

function inspect($varNom,$var){return gestLib_inspect($varNom,$var);}

// - accee aux libs - //
function debugShowVar($lib,$level,$line='',$methode='',$varNom,$var,$br=NULL)
	{if(isset($this->lib[$lib]))return $this->lib[$lib]->debugShowVar($level,$line,$methode,$varNom,$var,$br=NULL);}

}	// class gestionLibrairie

// - creation d'une instance prefedinie - //
$gestLib= new gestionLibrairies();
$gestLib->loadLib('gestLib',__FILE__,GESTLIBVERSION,'gestionnaire de librairies');
$gestLib->end('gestLib');

//echo  gestLib_inspect('gestLib',$gestLib,__LINE__);
?>
