//***************************************
version 0.1
2013.09.29
* initial commit: frise temporelle des eres geologiques

* correction de bug:
 - pas encore!
 
* fonctions ajoutes/modifications:
 - Frise A: synoptique general
 - Frise B: detail temporal de la frise A
 - ajout des periodes glaciaires/interglaciaires

* bugs restants: ?

//***************************************
toDo:
* ameliorer le menu
* ajouter des pages de details thematiques
* FriseB: creation d'une regle a interval adapte
* support de frise:ajuster (augmenter) la longueur a la largeur de la page (ou du parent)
* creer un object frise type avec les fonction standart a herite

//***************************************
2013.09.??
* correction de bug:
 - pas encore!
 
* fonctions ajoutes/modifications:
 - Frise A: synoptique general
 - Frise B: detail temporal de la frise A
 - ajout des periodes glaciaires/interglaciaires

* bugs restants: ?

//***************************************
toDo:
*1- FriseB: creation d'une regle a interval adapte
* ameliorer le menu
* ajouter des pages de details thematiques
* support de frise:ajuster (augmenter) la longueur a la largeur de la page (ou du parent)
* creer un object frise type avec les fonction standart a herite



