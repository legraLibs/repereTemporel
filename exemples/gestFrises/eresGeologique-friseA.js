/*!
gestion d'une frise specialiser pour afficher les eres geologiques
l'idHTML du support doit etre creer et la page chaerger avant de creer l'instance frise
*/

friseA=null;
function createFriseA()
{
friseA=new Tfrise({
	'idHTML':'friseA_support'
	,'supportRegles':'friseA_supportRegles'
	,'supportDatas':'friseA_supportDatas'
	,'bandeModele':{'date_deb': {'an': -4.6*ut.Milliard,'unite':'an'},'date_fin': {'an': 4000,'unite':'an'},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 30}
	});

friseA.styleInit=function()
	{
	// -- initialisation des couleurs -- //
	this.evtStyle=new Array();
	this.evtStyle.backgroundColor=new Array();
	// == superEons == //
	// == Eons == //
	this.evtStyle.backgroundColor['Hadeen']='#702';
	this.evtStyle.backgroundColor['Archeen']='#F22';
	this.evtStyle.backgroundColor['Proterozoique']='#F55';
	this.evtStyle.backgroundColor['Phanerozoique']='#F99';
	// == Eres == //
	this.evtStyle.backgroundColor['Eoarcheen']='#F20';
	this.evtStyle.backgroundColor['Paleoarcheen']='#F24';
	this.evtStyle.backgroundColor['Pesoarcheen']='#F28';
	this.evtStyle.backgroundColor['Neoarcheen']='#F2B';

	this.evtStyle.backgroundColor['Paleo']='#F50';
	this.evtStyle.backgroundColor['Meso']='#F58';
	this.evtStyle.backgroundColor['Neo']='#F5F';

	this.evtStyle.backgroundColor['Paleozoique']='#F59';
	this.evtStyle.backgroundColor['Cenozoique']='#F5C';

	// == Periodes == //
	this.evtStyle.backgroundColor['Siderien']='#55F';
	this.evtStyle.backgroundColor['Rhyacien']='#F22';
	this.evtStyle.backgroundColor['Orosirien']='#55F';
	this.evtStyle.backgroundColor['Statherien']='#F22';

	this.evtStyle.backgroundColor['Calymmien']='#55F';
	this.evtStyle.backgroundColor['Ectasien']='#F22';
	this.evtStyle.backgroundColor['Stenien']='#55F';

	this.evtStyle.backgroundColor['Tonien']='#F22';
	this.evtStyle.backgroundColor['Cryogemmien']='#55F';
	this.evtStyle.backgroundColor['Ediacarien']='#F22';

	this.evtStyle.backgroundColor['Cambrien']='#55F';
	this.evtStyle.backgroundColor['Ordovicien']='#F22';
	this.evtStyle.backgroundColor['Silurien']='#55F';
	this.evtStyle.backgroundColor['Devonien']='#F22';
	this.evtStyle.backgroundColor['Carbonifere']='#55F';
	this.evtStyle.backgroundColor['Permien']='#F22';
	this.evtStyle.backgroundColor['Trias']='#55F';
	this.evtStyle.backgroundColor['Jurrasique']='#F22';
	this.evtStyle.backgroundColor['Cretace']='#55F';
	this.evtStyle.backgroundColor['Paleogene']='#F22';
	this.evtStyle.backgroundColor['Neogene']='#55F';
	this.evtStyle.backgroundColor['Quaternaire']='#F22';

	// == Epoques == //
	this.evtStyle.backgroundColor['Sturtien']='#F22';
	this.evtStyle.backgroundColor['Varangien']='#55F';
	this.evtStyle.backgroundColor['Terreneuvien']='#F22';
	this.evtStyle.backgroundColor['Serie2']='#55F';
	this.evtStyle.backgroundColor['Serie3']='#F22';
	this.evtStyle.backgroundColor['Furongien']='#55F';
	this.evtStyle.backgroundColor['OrdovicienMoyen']='#F22';
	this.evtStyle.backgroundColor['OrdovicienSuperieur']='#55F';
	this.evtStyle.backgroundColor['Llandovery']='#F22';
	this.evtStyle.backgroundColor['Wenlock']='#55F';
	this.evtStyle.backgroundColor['Ludlow']='#F22';

	this.evtStyle.backgroundColor['DevonienInferieur']='#55F';
	this.evtStyle.backgroundColor['DevonienMoyen']='#F22';
	this.evtStyle.backgroundColor['DevonienSuperieur']='#55F';

	this.evtStyle.backgroundColor['Dinantien']='#F22';
	this.evtStyle.backgroundColor['Cisuralien']='#55F';
	this.evtStyle.backgroundColor['Guadalupien']='#F22';
	this.evtStyle.backgroundColor['Lopingien']='#55F';
	this.evtStyle.backgroundColor['TriasInferieur']='#F22';
	this.evtStyle.backgroundColor['TriasMoyen']='#55F';
	this.evtStyle.backgroundColor['TriasSuperieur']='#F22';

	this.evtStyle.backgroundColor['Lias']='#F22';
	this.evtStyle.backgroundColor['Dogger']='#55F';
	this.evtStyle.backgroundColor['Malm']='#F22';
	
	this.evtStyle.backgroundColor['CretaceInferieur']='#55F';
	this.evtStyle.backgroundColor['CretaceSuperieur']='#F22';
	
	
	this.evtStyle.backgroundColor['Paleocene']='#55F';
	this.evtStyle.backgroundColor['Eocene']='#F22';
	this.evtStyle.backgroundColor['Oligocene']='#55F';
	this.evtStyle.backgroundColor['Miocene']='#F22';
	this.evtStyle.backgroundColor['Pliocene']='#55F';

	this.evtStyle.backgroundColor['Pleistocerene']='#F22';
	this.evtStyle.backgroundColor['Holocene']='#55F';

	// == Etages == //
	this.evtStyle.backgroundColor['Fortunien']='#F22';
	this.evtStyle.backgroundColor['Etage2']='#55F';
	this.evtStyle.backgroundColor['Etage3']='#F22';
	this.evtStyle.backgroundColor['Etage4']='#55F';
	this.evtStyle.backgroundColor['Etage5']='#F22';
	this.evtStyle.backgroundColor['Drumien']='#55F';
	this.evtStyle.backgroundColor['Guzhangien']='#F22';
	this.evtStyle.backgroundColor['Paibien']='#55F';
	this.evtStyle.backgroundColor['Jiangshanien']='#F22';
	this.evtStyle.backgroundColor['Etage10']='#55F';
	this.evtStyle.backgroundColor['Tremadocien']='#F22';
	this.evtStyle.backgroundColor['Floien']='#55F';
	this.evtStyle.backgroundColor['Sandbien']='#F22';
	this.evtStyle.backgroundColor['Katien']='#55F';
	this.evtStyle.backgroundColor['Hirnantien']='#F22';
	this.evtStyle.backgroundColor['Rhuddanien']='#55F';
	this.evtStyle.backgroundColor['Aeronien']='#F22';
	this.evtStyle.backgroundColor['Telychien']='#55F';
	this.evtStyle.backgroundColor['Sheinwoodien']='#F22';
	this.evtStyle.backgroundColor['Gorstien']='#55F';
	this.evtStyle.backgroundColor['Ludfordien']='#F22';
	
	this.evtStyle.backgroundColor['Lochkovien']='#55F';
	this.evtStyle.backgroundColor['Fortunien']='#F22';
	this.evtStyle.backgroundColor['Praguien']='#55F';
	this.evtStyle.backgroundColor['Emsien']='#F22';
	this.evtStyle.backgroundColor['Eifelien']='#55F';
	this.evtStyle.backgroundColor['Givetien']='#F22';
	this.evtStyle.backgroundColor['Famennien']='#55F';
	this.evtStyle.backgroundColor['Tournaisien']='#F22';
	this.evtStyle.backgroundColor['Viseen']='#55F';
	this.evtStyle.backgroundColor['Serpoukhovien']='#F22';
	this.evtStyle.backgroundColor['Bashkirien']='#55F';
	this.evtStyle.backgroundColor['Moscovien']='#F22';
	this.evtStyle.backgroundColor['Kasimovien']='#55F';
	this.evtStyle.backgroundColor['Gzhelien']='#F22';
	
	
	this.evtStyle.backgroundColor['Asselien']='#F22';
	this.evtStyle.backgroundColor['Sakmarien']='#55F';
	this.evtStyle.backgroundColor['Artinskien']='#F22';
	this.evtStyle.backgroundColor['Kungurien']='#55F';
	this.evtStyle.backgroundColor['Roadien']='#F22';
	this.evtStyle.backgroundColor['Wordien']='#55F';
	this.evtStyle.backgroundColor['Capitanien']='#F22';
	this.evtStyle.backgroundColor['Wuchiapingien']='#55F';
	this.evtStyle.backgroundColor['Changhsingien']='#F22';

	this.evtStyle.backgroundColor['Induen']='#55F';
	this.evtStyle.backgroundColor['Olenekien']='#F22';
	this.evtStyle.backgroundColor['Anisien']='#55F';
	this.evtStyle.backgroundColor['Ladinien']='#F22';
	this.evtStyle.backgroundColor['Carnien']='#55F';
	this.evtStyle.backgroundColor['Norien']='#F22';
	this.evtStyle.backgroundColor['Rhetien']='#55F';
	}


friseA.auto=function()
	{
	this.creer_regleA();
//	this.creer_regleA_repereMa();
	this.creerReperesAuto({'callbacks':{'texte':'Million','title':'details'}});

	
//	this.creerReperesAuto({'repereNb':10});

	this.creer_bandeSuperEons();
	this.showEvt_Niveau('superEons',this.gestBandes.bandesData['superEons']);

/*	this.creer_bandeEons();
	this.showEvt_Niveau('Eons',this.gestBandes.bandesData['Eons']);
	this.creer_bandeEres();
	this.showEvt_Niveau('Eres',this.gestBandes.bandesData['Eres']);
	this.creer_bandePeriodes();
	this.showEvt_Niveau('Periodes',this.gestBandes.bandesData['Periodes']);
	this.creer_bandeEpoques();
	this.showEvt_Niveau('Epoques',this.gestBandes.bandesData['Epoques']);
	this.creer_bandeEtages();
	this.showEvt_Niveau('Etages',this.gestBandes.bandesData['Etages']);
*/
	}

// == function de creation des bandes: il utilise bandeModele == //
friseA.creer_regleA=function()
	{
	showActivity("creation de la regle: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'idHTML_parent':this.supportRegles,'idHTML': 'regleA','nom':'regleA','top':0,'hauteur':30,'title': 'regle principale (A)','genre': 'regle'},options);
	//showCode(regle);
	}

	// -- bande regleA: echelle:Ma :  -- //
	// === repere tous les 100Ma === //
friseA.creer_regleA_repereMa=function()
	{
	var regle=this.gestBandes.bandesRegle['regleA'];
	showActivity("regle: [ | ] (repere tous les 100Ma)",1);
	for(var regleNu=0;regleNu<11;regleNu++)
		{
		var an=-5*ut.Milliard+500*ut.Million*regleNu;
		var d=an/ut.Million;
		var evtNu=regle.evtAdd(
			 {'titre': d+'Ma','texte':d,'date_deb': {'an':an}}
			,{'classe': 'regleRepere'}
			);
		// == application du style personnel a l'evt == //
		var attr={};
		attr.click=function(frise,an)
		 			{
		 			alert('click sur repere:'+an,1);
		 			}
		attr.dblclick=function(frise,an)
		 			{
		 			alert('DOUBLE click sur repere:'+an,1);
		 			}
		this.gestBandes.bandesRegle[regle.nom].evt[regleNu].setAttrCSS(attr);
		}

	delete(a);
	}

friseA.creer_bandeSuperEons=function()
	{
	showActivity("creation de la bande: superEons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeSuperEons','nom':'superEons','top': 0,'hauteur':10,'title': 'bande Super&eacute;ons'},options);
	showCode(this.gestBandes.bandesData['superEons']);
	}

friseA.creer_bandeEons=function()
	{
	showActivity("creation de la bande: Eons",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEons','nom':'Eons','top': 10,'hauteur':30,'title': 'bande Eons'},options);
	showCode(this.gestBandes.bandesData['Eons']);
	}

friseA.creer_bandeEres=function()
	{
	showActivity("creation de la bande: Eres",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEres','nom':'Eres','top': 40,'hauteur':30,'title': 'bande Eres'},options);
	showCode(this.gestBandes.bandesData['Eres']);
	}

friseA.creer_bandePeriodes=function()
	{
	showActivity("creation de la bande: Periodes",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandePeriodes','nom':'Periodes','top': 70,'hauteur':30,'title': 'bande Periodes'},options);
	showCode(this.gestBandes.bandesData['Periodes']);
	}

friseA.creer_bandeEpoques=function()
	{
	showActivity("creation de la bande: Epoques",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEpoques','nom':'Epoques','top': 100,'hauteur':30,'title': 'bande Epoques'},options);
	showCode(this.gestBandes.bandesData['Epoques']);
	}

friseA.creer_bandeEtages=function()
	{
	showActivity("creation de la bande: Etages",1);
	var options=undefined;
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeEtages','nom':'Etages','top': 130,'hauteur':30,'title': 'bande Etages'},options);
	showCode(this.gestBandes.bandesData['Etages']);
	}


	// == function d'ajout des evt ==//
friseA.showEvt_Niveau=function(niveau,bandeRef)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=eresGeologique[niveau];
	if(!t)return;
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	var global_Tfrise_msg='ajout de des eres dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p])=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function()
			 	{
			 	alert('click sur evt:'+this.innerHTML,1);
			 	}
			attr.dblclick=function()
		 		{
		 		alert('DOUBLE click sur repere:'+this.innerHTML,1);
		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}

//initialisation
friseA.styleInit();

}	//createFriseA
