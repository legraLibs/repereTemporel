//*************************************************
// auteurs-regles_load.js
// (c)pascal TOLEDO http://www.legral.fr
// date 18 decembre 2011
// v1.0: 
// donnee sur les auteurs: Tolkien
//*************************************************

var regle_decenale_bande=new TrepereTemporel_bande(auteurs_bande,{'genre':'repere','idHTML':'regle_decenal'});

regle_decenale_bande.afficher(1);
//-----------------------------------------------------
regle_decenale_bande.evtAdd({'titre':'1890','date_debut':{'an':1890,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});

regle_decenale_bande.evtAdd({'titre':'1900','date_debut':{'an':1900,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});
regle_decenale_bande.evtAdd({'titre':'1910','date_debut':{'an':1910,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});
regle_decenale_bande.evtAdd({'titre':'1920','date_debut':{'an':1920,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});
regle_decenale_bande.evtAdd({'titre':'1930','date_debut':{'an':1930,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});
regle_decenale_bande.evtAdd({'titre':'1940','date_debut':{'an':1940,'ms':1,'jr':1}},{'classe':'Lovecraft_regle_trait','opacity':1});

