/*!
function Tfrise()
gestion d'une frise specialiser pour afficher les eres geologiques
*/
global_Tfrise_msg='';

function Tfrise(initialisation)
	{
	var init=initialisation?initialisation:{};
	// --- auteurs - creation de la bande reference/model  --- //
	showActivity("creation de la  frise",1);

	// -- initialisation des couleurs -- //
	this.evtStyle=eresGeologique.evtStyle;

	// == format de la bande qui sera herite == //
	showActivity("creation du modele de bande",1);

	this.bandeModele=new TrepereTemporel_bandeData	//cette bande servira de modele au bandes affichees dans la frise (ce modele lui meme n'est pas afficher) 
		({'date_deb': {'an': -4.6*ut.Milliard},'date_fin': {'an': 4000},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 30});
		if(typeof(this.bandeModele.toSource)==='function')showActivity(this.bandeModele.toSource(),1);
		showCode(this.bandeModele);

	this.auto=function(config)
		{
		var conf=config?config:{};
		
		this.creer_regleA();
		this.creer_regleA_repereMa();

		this.creer_bandeSuperEons();
			this.showEres_Niveau('superEons',this.gestBandes.bandesData['superEons']);
		this.creer_bandeEons();
			this.showEres_Niveau('Eons',this.gestBandes.bandesData['Eons']);
		this.creer_bandeEres();
			this.showEres_Niveau('Eres',this.gestBandes.bandesData['Eres']);
		this.creer_bandePeriodes();
			this.showEres_Niveau('Periodes',this.gestBandes.bandesData['Periodes']);
		this.creer_bandeEpoques();
			this.showEres_Niveau('Epoques',this.gestBandes.bandesData['Epoques']);
		this.creer_bandeEtages();
			this.showEres_Niveau('Etages',this.gestBandes.bandesData['Etages']);
		if(conf.show)this.show();
		}


	// == synchronisation des bandes == //
	this.recalcul=function()
		{
		showActivity("frise: recalcul",1);
		this.gestBandes.recalcul();
		}

	// == function de redimentionnement de la bandeModele == //
	this.addLeftMarge=function()
		{
		this.bandeModele.left_marge=30;
		document.getElementById('friseA_support').style.width=this.bandeModele.longueur+this.bandeModele.left_marge+'px';	//application de la marge gauche sans utiliser la fonction recalcul
		}

	this.longueurInc=function(lg)
		{
		this.bandeModele.longueurInc(lg);
		this.recalcul();
		}

	this.dateAdd=function(debFin,uniteNb,uniteType)
		{
		this.bandeModele.date_Add(debFin,uniteNb,uniteType);
		this.recalcul();
		}
	
	this.translate=function(uniteNb,uniteType)
		{
		this.bandeModele.translate(uniteNb,uniteType);
		this.recalcul();

		}
	this.limite=function(deb,fin)
		{
		this.bandeModele.limite(deb,fin);
		this.recalcul();
		}

	this.show=function()
		{
		document.getElementById('friseA_support').style.display='block';
		}
	this.hide=function()
		{
		document.getElementById('friseA_support').style.display='none';
		}


	// -- gestion des bandes-- //
	this.gestBandes= new TgestBandes(this);// ajout de la creation et gestion dynamiue des bandes pour la frise actuelle (this)

	// -- function de creation des regles -- //
	this.creer_regleA=function()
		{
		showActivity("creation de la regle: Eons",1);
		var options=undefined;
		this.gestBandes.creerBande({'idHTML_parent':'friseA_regles_support','idHTML': 'regleA','nom':'regleA','top':0,'hauteur':30,'title': 'regle principale (A)','genre': 'regle'},options);
		//showCode(regle);
		}


	// === repere tous les 100Ma === //
	this.creer_regleA_repereMa=function()
		{
		var regle=this.gestBandes.bandesRegle['regleA'];
		showActivity("regle: [ | ] (repere tous les 100Ma)",1);
		for(var regleNu=0;regleNu<11;regleNu++)
			{
			var an=-5*ut.Milliard+500*ut.Million*regleNu;
			var d=an/ut.Million;
			var evtNu=regle.evtAdd(
				 {'titre': d+'Ma','texte':d,'date_deb': {'an':an}}
				,{'classe': 'regleRepere'}
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.click=function(frise)
			 			{
			 			alert('click sur repere:'+null,1);
			 			}
			attr.dblclick=function(frise)
			 			{
			 			alert('DOUBLE click sur repere:'+null,1);
			 			}
			this.gestBandes.bandesRegle[regle.nom].evt[regleNu].setAttrCSS(attr);
			}
		}

	this.creer_bandeSuperEons=function()
		{
		showActivity("creation de la bande: superEons",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandeSuperEons','nom':'superEons','top': 0,'hauteur':10,'title': 'bande Super&eacute;ons'},options);
		//showCode(this.gestBandes.bandesData['superEons']);
		}

	this.creer_bandeEons=function()
		{
		showActivity("creation de la bande: Eons",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandeEons','nom':'Eons','top': 10,'hauteur':30,'title': 'bande Eons'},options);
		//showCode(this.gestBandes.bandesData['Eons']);
		}

	this.creer_bandeEres=function()
		{
		showActivity("creation de la bande: Eres",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandeEres','nom':'Eres','top': 40,'hauteur':30,'title': 'bande Eres'},options);
		//showCode(this.gestBandes.bandesData['Eres']);
		}

	this.creer_bandePeriodes=function()
		{
		showActivity("creation de la bande: Periodes",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandePeriodes','nom':'Periodes','top': 70,'hauteur':30,'title': 'bande Periodes'},options);
		//showCode(this.gestBandes.bandesData['Periodes']);
		}

	this.creer_bandeEpoques=function()
		{
		showActivity("creation de la bande: Epoques",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandeEpoques','nom':'Epoques','top': 100,'hauteur':30,'title': 'bande Epoques'},options);
		//showCode(this.gestBandes.bandesData['Epoques']);
		}

	this.creer_bandeEtages=function()
		{
		showActivity("creation de la bande: Etages",1);
		var options=undefined;
		this.gestBandes.creerBande({'genre': 'data',idHTML_parent:'friseA_bandes_support','idHTML': 'bandeEtages','nom':'Etages','top': 130,'hauteur':30,'title': 'bande Etages'},options);
		//showCode(this.gestBandes.bandesData['Etages']);
		}

	// == function d'ajout des evt:Eres ==//
	this.showEres_Niveau=function(niveau,bandeRef)
		{
		showActivity("ajoute un groupe d'evt",1);
		var t=eresGeologique[niveau];
		if(!t)return;
		if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

		var global_Tfrise_msg='ajout de des eres dans la bande '+bandeRef.idHTML;
		for(var p in t)
			{
			if(typeof(t[p])=='object')
				{
				var out=t;
				out=p;
				var EvtNu=bandeRef.evtAdd(
					t[p]
					,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
					,{
						 deb: function(t){showActivity(global_Tfrise_msg,'add')}
						,fin: function(t){showActivity(global_Tfrise_msg,'add')}
					 }
					);
				// == application du style personnel a l'evt == //
				var attr={};
				attr.texte=p;
				if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
				attr.click=function()
				 	{
//				 	alert('click sur evt:'+this.innerHTML,1);
					var d=document.getElementById('detailTitre');
					d.innerHTML=this.innerHTML;
				 	friseB.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
				 	
				 	// creation des reperes dans la limite
				 	var deb=null;
				 	var fin=null;
				 	var repereNb=10;
				 	//this.creer_ReperesAuto(regleNom,deb,fin,repereNb,config);
				 	}
				attr.dblclick=function()
			 		{
			 		alert('DOUBLE click sur evt:'+this.innerHTML,1);
			 		}
				this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
				}
			}
		}


	return this;
	}	// TFriseA()
