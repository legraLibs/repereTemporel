/* - ./locales/scripts.js: fichier generer automatiquement - */

/*! - ./repereTemporel-gestFrises.js - */
if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel-gestFrises',ver:'1.3.0',description:"gestion des frises",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel-gestFrises.js
version: cf release
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2014.11.01
role: gestion d'une frise dans son ensemble en fournissant des fonctions avancees
dependance:
	* gestLib(facultatif)
	* reperetemporel.js
	* repereTemporel-gestBandes.js
*/


/*******************************************************
 **** objet: gestion de frise  ****
// 
********************************************************/

global_Tfrise_msg=[];  // messages pour les fonctions anonymes

function Tfrise(initialisation)	{
	var init=initialisation?initialisation:{};

	// -- validation de la balise HTML du support de la frise -- //
	if(!init.idHTML)return null;	// a modifier en generer 1 si non defini 
	this.idHTML=init.idHTML;
	this.idCSS=document.getElementById(this.idHTML);
	if(!this.idCSS)return null;

	this.supportRegles=init.supportRegles?init.supportRegles:undefined;	// (idHTML) A FAIRE: supportRegles personalise
	this.supportDatas= init.supportDatas? init.supportDatas:undefined;	// (idHTML) A FAIRE: supportDatas personalise

	this.supportReglesIdCSS=document.getElementById(this.supportRegles);
	this.supportDatasIdCSS= document.getElementById(this.supportDatas);

	// -- ajout de la bande modele -- //
	if(typeof(init.bandeModele)!=='object')return null;
	
	// -- Creation de la  bande qui servira de modele au bandes affichees dans la frise (ce modele lui meme n'est pas affiche) -- //
	this.bandeModele=new TrepereTemporel_bandeData(init.bandeModele);

	// -- gestion des bandes -- //
	this.gestBandes= new TgestBandes(this);// ajout de la creation et gestion dynamique des bandes pour la frise actuelle (this)

	// *** fonctions non prototyper qui sont a surcharger par l'utilisateur *** //
	// -- exemple de modele de fonction de demarrage -- //
	//this.auto=function(config){var conf=config?config:{};}

	// -- redimentionner la frise selon la bande modele -- //
	this.idCSS.style.width=this.bandeModele.longueur+'px';
	if(this.supportReglesIdCSS)this.supportReglesIdCSS.style.width=this.idCSS.style.width;
	if(this.supportDatasIdCSS) this.supportDatasIdCSS.style.width =this.idCSS.style.width;


	//this.idCSS.style.height=this.bandeModele.hauteur+'px'; // egale a la somme des hauteurs des bandes

	// -- recalcul synchronisation des bandes -- //
	this.recalcul=function(config){this.gestBandes.recalcul(config);}
	return this;
	}

// *** fonctions prototyper: communs a totues les frises *** //

Tfrise.prototype={
	// == function de redimentionnement de la bandeModele == //
	addLeftMarge:function(marge)
		{
		if(isNaN(marge))return 0;
		this.bandeModele.left_marge=marge;
		this.idCSS.style.width=this.bandeModele.longueur+this.bandeModele.left_marge+'px';	//application de la marge gauche sans utiliser la fonction recalcul
		return this.bandeModele.left_marge;
		}

	,longueurInc:function(lg)
		{
		this.bandeModele.longueurInc(lg);
		this.recalcul();
		return this;
		}

	,dateAdd:function(debFin,uniteNb,uniteType)
		{
		this.bandeModele.date_Add(debFin,uniteNb,uniteType);
		this.recalcul();
		return this;
		}
	
	,translate:function(uniteNb,uniteType)
		{
		this.bandeModele.translate(uniteNb,uniteType);
		this.recalcul();
		return this;
		}
	,limite:function(deb,fin)
		{
		this.bandeModele.limite(deb,fin);
		this.recalcul();
		return this;
		}

	,show:function(){this.idCSS.style.display='block';return this.idCSS.style.display;}
	,hide:function(){this.idCSS.style.display='none';return this.idCSS.style.display;}

	// == function d'ajout dans la bande bandeRef de donnee (tableau remplit de dateTime)  ==//
	/*
	 * exemple
	,showData:function(bandeRef,donnees,evtConfig,cb)
		{
//		showActivity("ajoute un groupe d'evt",1);
//		if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}
		if(typeof(bandeRef)!=='object')return;

		var evtConf=(typeof(evtConfig)=='object')?evtConfig:{};
		evtConf.opacity=evtConf.opacity?evtConf.opacity:1;
		evtConf.classe=evtConf.classe?evtConf.classe:'';
		evtConf.zIndex=evtConf.zIndex?zIndex.opacity:1005;

		// gestion des callBacks
		if(typeof(cb)!='object')cb={};
		if(typeof(cb.deb)!='function')cb.deb=function(){};
		if(typeof(cb.fin)!='function')cb.fin=function(){};
			
		global_Tfrise_msg[this.idHTML]='ajout de de la prehistoire dans la bande '+bandeRef.idHTML;
		for(var p in donnees)
			{
			if(typeof(donnees[p])=='object')
				{
				//var out=t;
				out=p;
				var EvtNu=bandeRef.evtAdd(
					donnees[p]
					,{'classe': evtConf.classe,'opacity': evtConf.opacity,'zIndex': evtConf.zIndex}
					,cb
//					,{
//						 deb: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
//						,fin: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
//					 }
//					);
				// == application du style personnel a l'evt == //
				var attr={};
				attr.texte=p;
//				if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
				attr.click=function()
				 	{
//				 	alert('click sur evt:'+this.innerHTML,1);
					var d=document.getElementById('detailTitre');
					d.innerHTML=this.innerHTML;
				 	this.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
				 	
				 	// creation des reperes dans la limite
				 	var deb=null;
				 	var fin=null;
				 	var repereNb=10;
				 	//this.creer_ReperesAuto(regleNom,deb,fin,repereNb,config);
				 	}
//				attr.dblclick=function()
//			 		{
//			 		alert('DOUBLE click sur evt:'+this.innerHTML,1);
//			 		}
				this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
				}
			}
		}
	*/
	// == function d'ajout dans la bande bandeRef de donnee (tableau remplit de dateTime)  ==//
	,creerReperesAuto:function(config){return this.gestBandes.creerReperesAuto(config);}
	,creerBande:function(b1,b2){return this.gestBandes.creerBande(b1,b2);}
	,addEvtInBande:function(bandeGenre,bandeNom,evtData,evtOpt,cb){return this.gestBandes.addEvtInBande(bandeGenre,bandeNom,evtData,evtOpt,cb);}

	} // TFrise.prototype()
if(typeof(gestLib)==='object')gestLib.end('repereTemporel-gestFrises');

/*! - ./repereTemporel-gestBandes.js - */
if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel-gestBandes',ver:'1.3.0',description:"gestion des bandes",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel-gestBandes.js
version: cf release
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2014.11.01
role: creation et gestion dynamique des bandes de type 'bandeRegle' ou 'bandeData'
dependance:
	* gestLib(facultatif)
	* reperetemporel.js

	
liste des functions:
 * TgestBandes(frise) : frise:objet TFrise
 * TgestBandes.prototype.creerBande=function(bandeConf,options): creait (statiquement ou dynamiquement) une bande et le met dans le tableau bandesRegle ou bandesData selon le type


Terminologie:
une bande est de 2 types: regles ou data (repereTemporel ne fait pas de distinction de type, c'est repereTemporel-gestBandes.js qui le fait)
Dans une bandeRegle sont ajoutes les elements de genre 'repere'
Dans une bandeData  sont ajoutes les elements de genre 'evt'

*/



/*******************************************************
 **** objet: gestion des bandes (regle ou evt) ****
 // crait de nouvelle instance de bande et les stocke dans un tableau
 // un indice tableau (identificateur) peut etre donner a charge au client a ce qu'il soit unique ds le tableau
 // (peut ecraser ou etre rejete au choix du client en cas de doublont; par defaut, rejeter)
 // si aucun indice donner, un pseudo aleatoire sera creer (a charge au client de recuperer cette indice
 // 
********************************************************/
function TgestBandes(frise){
	if(typeof(frise)!=='object')return null;
	this.frise=frise;
	this.bandesRegle=new Array();
	this.bandesData=new Array();
	//this.positions=new array();//ordre d'affichage des bandes //a faire
	return this;
	}

TgestBandes.prototype.recalcul=function(config){
	for(var bRef in this.bandesRegle) this.bandesRegle[bRef].recalcul(config);
	for(var bRef in this.bandesData) this.bandesData[bRef].recalcul(config);
	return 1;
	}

// renvoie le nom de la 1ere regle
TgestBandes.prototype.getRegleNom=function(){
	for(var n in this.bandesRegle)if(typeof(this.bandesRegle[n])!='undefined')return n;
	return '';
	}


//  -- crait une bande -- //
// l'element <div> est creer a partir de l'idHTML donnee, sinon une idHTML est creer aleatoirement (ou si idHTML='auto')
// retourne bc.nom (le nom calculer)
// surcharge: surchage certains parametres et options
// - options (dans surcharge)
// - - min: interval mini lors de la generation de l'idHTML
// - - max: interval maxi lors de la generation de l'idHTML
TgestBandes.prototype.creerBande=function(bandeOri,surcharge){
	if(typeof(bandeOri)!=='object')return null;

	// - gestion de la surcharge - //
	var bf=bandesFusion(bandeOri,surcharge);

	var bandeNu=null;
	
	if(!bf.genre)bf.genre='evt';

	// - calcul de l'idHTML - //
	if(!bf.idHTML || bf.idHTML == "auto")
		{
		var min=(!isNaN(bf.min))?bf.min:0;
		var max=(!isNaN(bf.max))?bf.max:1000000;
		var nu=min+Math.floor((max-min+1)*Math.random());
		var rt=(bf.genre=='evt')?'rt_bandeData_':'rt_bandeRegle_';
		bf.idHTML=rt+nu.toString();
		}


	// - recherche de l'id de la bandeParent - //
	// 1- bf.idHTML_parent
	// 2- this.supportDatas
	var idHTML_bandeParent=
		(document.getElementById(bf.idHTML_parent))
			?document.getElementById(bf.idHTML_parent)
			:(bf.genre=='evt')
				?document.getElementById(this.frise.supportDatas)
				:document.getElementById(this.frise.supportRegles)
				;

	// - recherche (calcul) du nom . Initialise bc.nom neccessaire pour le retrouver lors de scan tableau bande[???].bc.nom - //
	// defini par "nom" sinon par idHTML
	bf.nom=bf.nom?bf.nom:bf.idHTML;

	// - recupere l'idCSS s'il existe sinon le crée - //
	this.idCSS=document.getElementById(bf.idHTML);
	if(!this.idCSS)
		{
		this.idCSS=document.createElement('div');
		// -- inserer l'element dans le parent -- //
		this.idCSS_parent=idHTML_bandeParent;
		this.idCSS_parent.appendChild(this.idCSS); 
		}
	this.idCSS.setAttribute('id',bf.idHTML);
	// - crait la bande - //
	// (et met a jours le tableau de ref concernes des idHTML de la frise)
	var bandeDest=(bf.genre=='evt')?this.bandesData:this.bandesRegle;
	bandeDest[bf.nom]=new TrepereTemporel_creerBande(this.frise.bandeModele,bf);	// ne gere pas la surcharge (ici bc est bc surcharger par surcharge
	//si erreur 
	return bf.nom;
	}


// == supprime tous les evt d'une regle == //
TgestBandes.prototype.RegleClean=function(regleNom){
	if(typeof(this.bandesRegle[regleNom])=='undefined')return null;
	
//	this.bandesRegle[regleNom].evt=[];
//	this.bandesRegle[regleNom].evtNb=0;
	this.bandesRegle[regleNom].evtSupprAll();
	return 1;
	}


// == creer_ReperesAuto : creait automatique n reperes entre deb et fin  == //
// regleNom
// repereNb: nombre de reperes
// deb,fin: {'an':an,'ms':ms, etc}
// callBack={} ex {'title':'indexTableauDuTitleVoulu'}
TgestBandes.prototype.creerReperesAuto=function(config){
	var conf=config?config:{};
	var rn=conf.regleNom?conf.regleNom:this.getRegleNom();
	var regle=this.bandesRegle[rn];
	if(!regle)return null;

	var callbacks=typeof(config.callbacks=='object')?config.callbacks:{};

	if(conf.cleanAll)this.RegleClean(rn); 
	conf.repereNb=isNaN(conf.repereNb)?3:conf.repereNb;

	var deb=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.deb):new TrepereTemporel_dateTime(regle.bandeData.date_deb);
	var fin=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.fin):new TrepereTemporel_dateTime(regle.bandeData.date_fin);

	var uniteNb=Math.abs(fin.diff(deb));
	if(uniteNb===0)return null;
	var distance=uniteNb/conf.repereNb;

	var dtTemp=new TrepereTemporel_dateTime(deb);

	var classe=conf.classe?conf.classe:'';
		
	for(var regleNu=0;regleNu<conf.repereNb;regleNu++)
		{
		var dtEvt=new TrepereTemporel_dateTime(dtTemp);
		var d=dtEvt.date_str();
		var evtNu=regle.evtAdd(
			 {'titre': d,'texte':d,'date_deb': dtEvt,'callbacks':callbacks}
			,{'classe': classe}
			);
			dtTemp.addUnite(distance);
		}
	return this;
	}

TgestBandes.prototype.addEvtInBande=function(bandeGenre,bandeNom,evtData,evtOpt,cb){
	var bandeDest=(bandeGenre=="regle")?this.bandesRegle[bandeNom]:this.bandesData[bandeNom];
	if (!bandeDest)return null;
	var EvtNu=bandeDest.evtAdd(evtData,evtOpt,cb);
	return EvtNu;
	}

if(typeof(gestLib)==='object')gestLib.end('repereTemporel-gestBandes');

/*! - ./repereTemporel.js - */
if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel',ver:'1.3.0',description:"afficheur de graphe temporel",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel.js
version: cf gestLib
auteur:pascal TOLEDO
date de creation: 2011.12.22
date de modification: 2014.11.01
role: affiche une barre de repere temporel
dependance:
	* gestLib(facultatif)
*/



ut={Million:1000000,Milliard:1000000000};

// instanciateur de TrepereTemporel_dateTime
// verifie s'il est deja une instance de ce type si oui le renvoie sinon le crait
function TrepereTemporel_new_dateTime(data)
	{
	if(!data)return null;
	if(typeof(data.isDateTimeObj)=='function')return data;
	return new TrepereTemporel_dateTime(data);
	}

/*******************************************************
class de gestion des datesTime neccessaire a l'evenement: class de gestion du contenu
// objt{an:00,ms:0,jr:0,hr:0,mn:0,sc:0}
//unite:'an','ms','jr','hr','mn','sc'
// instantcie en ''
********************************************************/
function TrepereTemporel_dateTime(dt)
	{
	this.an=this.ms=this.jr=this.hr=this.mn=this.sc=null;
	this.unite='jr';

	// - initialisation des valeurs de temps - //
	if(typeof(dt)=='object')
		{
		if(!isNaN(dt.an))this.an=dt.an;
		if(!isNaN(dt.ms))this.ms=dt.ms;
		if(!isNaN(dt.jr))this.jr=dt.jr;
		if(!isNaN(dt.hr))this.hr=dt.hr;
		if(!isNaN(dt.mn))this.mn=dt.mn;
		if(!isNaN(dt.sc))this.sc=dt.sc;
		this.unite=(dt.unite!=undefined)?dt.unite:'jr';//unite par defaut:jours
		}

	// - si date acteulle demander on surcharge les valeurs - //
	if(dt=="now") this.setNow();

	return this;
	}
TrepereTemporel_dateTime.prototype={
	isDateTimeObj:function(){return 1;}
	,isNull:function(){return((this.an===null)&&(this.ms===null)&&(this.jr===null)&&(this.hr===null)&&(this.mn===null)&&(this.sc===null));}
	,date_str:function()
		{
		if(this.isNull())return null;
		var out='';
		if(this.jr)out+=this.jr+'/';if(this.ms)out+=this.ms+'/';if(this.an)out+=this.an+' ';
		if(this.hr)out+=this.hr+'h';if(this.mn)out+=this.mn+'m';if(this.sc)out+=this.sc+'s';
		return out;
		}
	,dt2Unite:function(unite){
		if(this.isNull())return null;
		unite=unite?unite:this.unite;
		var UniteQt=0;
		switch(unite)
			{
			case'an':if(this.an)UniteQt=this.an;
				break;

			case	'jr':
				//nbUnite= tps_dt_jour + tps_dt_mois*30.5 + tps_dt_annee*365;
				if(this.an)UniteQt+=this.an*365.25;
				if(this.ms)UniteQt+=this.ms*30.5;
				if(this.jr)UniteQt+=this.jr;
//				if(this.hr)UniteQt+=this.hr;
//				if(this.mn)UniteQt+=this.mn;
//				if(this.sc)UniteQt+=this.sc;
				break;
			}
		return UniteQt;
		}
	// renvoie la difference, en unite de temps, entre dt - this. La date de l'object n'est pas modifie.
	// dt_comp doit etre de Type TrepereTemporel_dateTime
	,diff:function(dt_comp){
		var out=0;
		if(dt_comp&&!dt_comp.isNull())out+=dt_comp.dt2Unite(this.unite)-this.dt2Unite();else out=null;
		return out;
		}
	// ajoute (ou soustrait) une quantite a l'unite preciser en argument ou par defaut a l'unite actuelle
	// retourne null en cas d'erreur (laisser) sinon le nombre d'unite ajouter (ou soustrait
	,addUnite:function(uniteNb,unite){
		if(isNaN(uniteNb))return null;
		unit=unite?unite:this.unite;
		switch(unit)
			{
			case	'an':this.an+=uniteNb;return uniteNb;break;
			case	'ms':this.ms+=uniteNb;return uniteNb;break;
			case	'jr':this.jr+=uniteNb;return uniteNb;break;
			case	'hr':this.hr+=uniteNb;return uniteNb;break;
			case	'mn':this.mn+=uniteNb;return uniteNb;break;
			case	'sc':this.sc+=uniteNb;return uniteNb;break;
			}
		return 0; // aucune unite ajoutee
		}

	// - setNow - //	
	,setNow:function(unite){
		var d=new Date();
		var dt={"an":d.getFullYear(),"ms":d.getMonth()+1,"jr":d.getDate(),"hr":d.getHours(),"mn":d.getMinutes(),"sc":d.getSeconds()};
		return this.setDT(dt,unite);
		}


	// setDT: set les valeurs
	// dt: donnees au format dt ({an,ms,js,hr,mn,sc})
	// zero:les valeurs non declarees sont mise a zero sinon elles ne sont pas modifies
	,setDT:function(dt,unite){
		if(typeof(dt)!=='object')return null;
		if(!dt.zero)dt.zero=0;
		this.an=dt.an?dt.an : dt.zero?null:this.an;
		this.jr=dt.jr?dt.jr : dt.zero?null:this.jr;
		this.ms=dt.ms?dt.ms : dt.zero?null:this.ms;
		this.hr=dt.hr?dt.hr : dt.zero?null:this.hr;
		this.mn=dt.mn?dt.mn : dt.zero?null:this.mn;
		this.sc=dt.sc?dt.sc : dt.zero?null:this.sc;
		return this;
		}
	
	} //EOf TrepereTemporel_dateTime.prototype

/******************************************************
 * bandesFusion(bandeOri,surcharge)
 * surcharge la bandeOri avec la surcharge et renvoie le nouvel objet
 * bandeOri et surcharge ne sont pas modifie
********************************************************/
function bandesFusion(bandeOri,surcharge){
	if(typeof(bandeOri)!='object')return {};

	// - copie de la bandeOriginal - //
	var fusion={};for(var i in bandeOri){fusion[i]=bandeOri[i];}

	// - surcharge - //
	if(typeof(surcharge)=='object')for(var i in surcharge){fusion[i]=surcharge[i];}
	return fusion;
	}

/*******************************************************
class de gestion des bandes: (dates,title,et du decalage vers la gauche)
instancie en 'bande' utiliser par les bandes (utilise ts les champs et les evt utilisation partiel)
********************************************************/
function TrepereTemporel_bandeData(bande){
	if(!bande)return null;
	this.date_deb=TrepereTemporel_new_dateTime(bande.date_deb);
	this.date_fin=TrepereTemporel_new_dateTime(bande.date_fin);
	if (this.date_fin.isNull()){this.date_fin=null;this.date_fin=this.date_deb;}
	this.title=     ((bande.title!=undefined)&&(bande.title!=''))?bande.title:'['+this.date_deb.date_str()+'|'+this.date_fin.date_str()+']';
	this.top=       (bande.top     !=undefined)?bande.top:0;
	this.left_marge=(bande.left_marge!=undefined)?bande.left_marge:0;
	this.hauteur=   (bande.hauteur !=undefined)?bande.hauteur:10;
	this.longueur=  (bande.longueur!=undefined)?bande.longueur:0;
	this.left=0;//a supprimer?
	return this;
	}
TrepereTemporel_bandeData.prototype={
	// ---- gestion des redimentionnements ----
	longueurInc:function(lg){
		if(!lg)return null;
		this.longueur+=lg;
		return this;
		}
	// ajoute ou soustrait un nombre d'unite a la date d'un type d'unite donnee
	,date_Add:function(debFin,uniteNb,uniteType){
		switch(debFin){
			case'deb':this.date_deb.addUnite(uniteNb,uniteType);break;
			case'fin':this.date_fin.addUnite(uniteNb,uniteType);break;
			}
		return this;
		}
	//deplacement relative des limites vers le passe
	,translate:function(uniteNb,uniteType,callbacks){
		this.date_Add('deb',uniteNb,uniteType);
		this.date_Add('fin',uniteNb,uniteType,callbacks);
		return this;
		}
	//fixation des limites deb et fin
	,limite:function(deb,fin){
		this.date_deb.setDT(deb);
		this.date_fin.setDT(fin);
		return this;
		}
	}// TrepereTemporel_bandeData.prototype



/*******************************************************
 **** objet: bande ****
 // bandeModele: bande qui sert de modele commun
 // bandeConf: personalisation de la bande  - surcharge le modele
 // bandeConf.IdCSS=document.createElement('span');
 en cas de non creation renvoie undefined (le tableau receveur aura une valeur undefined)
 // Attention idHTML est requis mais n'est pas creer automatiquement (fais par gestBandes)
********************************************************/
function TrepereTemporel_creerBande(bandeModele,bandeConf){
	if(!bandeModele)return undefined;
/*
	this.idHTML=(bandeConf!=undefined)?bandeConf.idHTML:null;
	this.nom=bandeConf.nom?bandeConf.nom:this.idHTML;
	this.idCSS=document.getElementById(this.idHTML);	// attention a null!
	if(!this.idCSS)return undefined;			// sila balise idHTML n'existe pas! impossible de continuer
*/
	// - fusionner les bandes - //
	var bf=bandesFusion(bandeModele,bandeConf);

	this.idHTML=bf.idHTML;
	this.nom=bf.nom?bf.nom:this.idHTML;
	this.idCSS=document.getElementById(this.idHTML);	// attention a null!
	if(!this.idCSS)return undefined;	

	this.idJS=this;	//pointeur sur l'object js lui-meme (non utiliser)

//	this.genre="evt";
//	if(typeof(bandeConf)==='object')if(bandeConf.genre)this.genre=bandeConf.genre;
	this.genre=bf.genre?bf.genre:"evt";


	// -- creation de la bande:on instancie une nouvelle bande a partir du modele -- //
	//this.bandeData=new TrepereTemporel_bandeData(bandeModele);
	this.bandeData=new TrepereTemporel_bandeData(bf);

	// -- ajout des classes -- //
	//this.classAdd(bandeModele.classes);
	this.classAdd(bf.classes);

	// -- personalisation (surcharge) de la bande -- //
/*	if(typeof(bandeConf)==='object')
		{
		if(bandeConf.titre)this.bandeData.titre=bandeConf.titre;
		if(bandeConf.title)this.bandeData.title=bandeConf.title;
		if(bandeConf.top)this.bandeData.top=bandeConf.top;
		if(bandeConf.longueur)this.bandeData.longueur=bandeConf.longueur;
		if(bandeConf.hauteur)this.bandeData.hauteur=bandeConf.hauteur;
		this.classAdd(bandeConf.classes);
		}
*/
		this.evtNb=0;
	this.evt=Array();
	this.recalcul();
	return this;
	}

//TrepereTemporel_bande=TrepereTemporel_creerBande;	// retroCompatibilite


TrepereTemporel_creerBande.prototype={
	afficher:function(visible){
		var v=visible?visible:1;// par defaut on affiche
		this.idCSS.style.display='display:'+v?'block':'none';
		return this;
		}
	,readHeight:function(){return((this.idCSS.style.height==='')?0:parseInt(this.idCSS.style.height,10));}

	,recalcul:function(param){
		if(typeof(param)==='object')
			{
			if(param.title)this.bandeData=param.title;
			if(!isNaN(param.left))this.bandeData.left_marge=param.left;
			if(!isNaN(param.top))this.bandeData.top=param.top;
			if(!isNaN(param.hauteur))this.bandeData.hauteur=param.hauteur;
			if(!isNaN(param.longueur))this.bandeData.longueur=param.longueur;
			if(typeof(param.deb)==='object')this.bandeData.date_deb=new repereTemporel_dateTime(param.deb);
			if(typeof(param.fin)==='object')this.bandeData.date_fin=new repereTemporel_dateTime(param.fin);
			}
			
		this.tps_ut_deb=this.bandeData.date_deb.dt2Unite();
		this.tps_ut_fin=this.bandeData.date_fin.dt2Unite();
		this.tps_ut_dur=Math.abs(this.tps_ut_fin-this.tps_ut_deb);

		this.ratio=(this.bandeData.longueur/this.tps_ut_dur);
		this.left=Math.round(this.tps_ut_deb*this.ratio);	//set pour les evt-fils
		if(this.idCSS)
			{
			this.idCSS.style.position='absolute';
			this.idCSS.title 	=this.bandeData.title;
			this.idCSS.style.left 	=this.bandeData.left_marge+'px';
			this.idCSS.style.top  	=this.bandeData.top+'px';
			this.idCSS.style.height	=this.bandeData.hauteur+'px';
			this.idCSS.style.width	=this.idCSS.style.minWidth=this.idCSS.style.maxWidth=this.bandeData.longueur+'px';
			// Pas de gestion des CSS de mise en forme (utilisation du #htmlID ds les feuilles de styles)
			}	
		for(var evtNu in this.evt)this.evt[evtNu].recalcul(param);//reclacul des evt-enfants
		return this;
		}

	// ---- gestion des evenements ----
	/*
	// evtAdd:function()
	// ajoute un span dans la bande et dans la pile d'evt 
	// return evtNu: le numero attribue a l'evt
	// cb: callback scenaristique cb AVANT et APRES la CAION/AFFICAHGE de  l'evt
	// ATTENTION; evtData contient aussi un callBack (mais n'est pas traite ici
	*/
	,evtAdd:function(evtData,evtOpt,cb){
		this.cb=cb?cb:{};
		if(typeof(this.cb.deb)=='function')this.cb.deb();
		var evtNu=this.evtNb;
		//creer et recuperer idObj
		this.evt[evtNu]=new TrepereTemporel_evt(evtData,evtOpt,{idHTML:this.idHTML,idJS:this.idJS});
		if(this.evt[this.evtNb]){this.evt[evtNu].afficher(1,this.ratio);this.evtNb++;}
		if(typeof(this.cb.fin)=='function')this.cb.fin();
		return evtNu;
		}

	,evtSupprAll:function(){for(var evtId in this.evt)this.evtSuppr(evtId);return this;}		
	,evtSuppr:function(evtId){
		var evt=this.evt[evtId];
		if(typeof(evt)!='undefined')
			{
			//suppression de l'entree du dom
			this.idCSS.removeChild(evt.idCSS); // provoque une exeption si pas son fils (doc DOM)
			//suppression de l'entree du tableau
			this.evt[evtId]=undefined;	//laisser this.evt[evtId]
			this.evtNb--;
			}
		return this;
		}		
		
		
	// --- gestion des evenements : Affichage ---
	,evtAfficher:function(evtNu,visible){
		if(!visible)visible=1;	//si non defini alors par defaut on affiche
		this.evt[evtNu].afficher(visible);	
		return this;
		}
	,classAdd:function(classes){
		if(classes)this.idCSS.className+=" "+classes;
		return this.idCSS.className;
		}
	}	// TrepereTemporel_bande.prototype


/*******************************************************
class neccessaire a l'evenement: class de gestion du contenu
// venant d'un fichier ou d'une DB
// aucune mise en forme ici !!!
// instancie en 'evtData'
********************************************************/
function TrepereTemporel_evtData(evtData){
	if(!evtData)return null;
	if(typeof(evtData.isEvtData)=='function')return evtData;

	cb=typeof(evtData.callBack)=='object'?evtData.callBack:{};

	//----  protection  ---- //
	if(typeof(evtData.date_deb)!=='object')return null;
	//----  fonctions de verification  ---- //
	this.isEvtData=function(){return 1;}
	this.typeOf=function(){return'TrepereTemporel_evtData';}

	
	//---- calcul  du texte/title ----//   EN COURS D MODIF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	this.date_deb=TrepereTemporel_new_dateTime(evtData.date_deb);
	if(!evtData.date_fin)evtData.date_fin=evtData.date_deb;
	
	this.date_fin=TrepereTemporel_new_dateTime(evtData.date_fin);
	var duree=this.date_deb.diff(this.date_fin);

	if((!this.date_fin)||(this.date_fin.isNull())){this.date_fin=null;this.date_fin=this.date_deb;}

	this.titre=(evtData.titre)?evtData.titre:'';
	var dt_fin_txt=(duree)	// si diff
		?this.date_deb.date_str()+'|'+this.date_fin.date_str()+'(duree:'+duree+')'	// on afffiche les 2
		:this.date_deb.date_str();						// on affiche debut seul

	this.title=((evtData.title)&&(evtData.title!=''))
		?this.titre+' '+evtData.title
		:this.titre+'['+dt_fin_txt+']';

	if(evtData.texte)this.texte=evtData.texte;
	if(!this.texte)this.texte='&nbsp';//&nbsp : obligatoire!!!
	if(typeof(cb.title)=="function")cb.title();

	//----  gestion de l'url  ---- //
	this.sourceTxt=(evtData.sourceTxt)?evtData.sourceTxt:'';
	this.sourceURL=(evtData.sourceURL)?evtData.sourceURL:'';

	return this;
	}



/*******************************************************
 **** objet: evenement ****
- ajoute un span-evt dans la bande donne en parametre
- evtData: initialise les variables contenu dasn le type evtData
- evtData.callBack:objet contenant les indices des tableaux pour appelle la fonction de callback
	ex:{'title':'milliard' }
- evtOpt: initialise toutes les autres var

********************************************************/
function TrepereTemporel_CreerEvt(evtData,evtOpt,parent){
	if(typeof(evtData)!=='object' || evtData=={} || typeof(parent)!=='object')return undefined;

	// -- protection d'exclusion -- //
	if(typeof(evtData)!=='object')return null;
	// -- initialisation -- //
	this.idHTML_parent=(parent)?parent.idHTML:null;
	this.idJS_parent=(parent)?parent.idJS:null;
	this.idCSS_parent=document.getElementById(this.idHTML_parent);//! element CSS
	this.idCSS=null;					// id de l'element CSS inserrer

	//-- initialisation des var calculees -- //
	this.tps_ut_deb=0;	//en unite de temps
	this.tps_ut_fin=0;	//en unite de temps
	this.tps_ut_dur=0;
	this.left=0;		//origine:left
	this.longueur=0;	//longueur:width

	this.evtData=new TrepereTemporel_evtData(evtData);
	
	if(typeof(evtOpt)=='object')
		{
		this.zIndex=evtOpt.zIndex?evtOpt.zIndex:1001;
		this.classes=evtOpt.classes?evtOpt.classes:'';
		this.opacity=evtOpt.opacity?evtOpt.opacity:1;	//a verifier si c'est bien un float ou int
		this.hauteur=evtOpt.hauteur?evtOpt.hauteur:this.idJS_parent.readHeight();
		}	
	//! Le comportement en tant qu'evt n'est active uniquement si un parent est donnee
	if(!parent)return null;

	
	// --  creation de la div this.creerCSS(); -- //
	this.idCSS=document.createElement('div');
		//this.idCSS.setAttribute('', 'mypage.htm');
	this.idCSS_parent.appendChild(this.idCSS); 

	// ==  initilialisation de creation == //
	this.idCSS.style.position='absolute';
	this.idCSS.style.overflow='hidden';
	this.idCSS.style.top=0;
	this.idCSS.style.zIndex=this.zIndex;
	this.idCSS.style.height =this.hauteur+'px';
	this.idCSS.style.cursor='help';
	this.idCSS.style.opacity=this.opacity;

	if(this.classes!='')this.idCSS.className+=' '+this.classes;

	this.idCSS.innerHTML=this.evtData.texte;
	this.idCSS.title=this.evtData.title;
		
	// ==  initialisation des callBAks standart (prototyper)  == //
	this.callbacks=[];
	this.callbacks['title']=[];
	this.callbacks['texte']=[];
	this.callbacksInit();
	
	// ==  gestion des Events  == //
	this.click=typeof(evtData.click)==='function'?evtData.click:function(){};

	
	// --  recalcul -- //
	this.recalcul(evtData);
	
	return this;
	}

//TrepereTemporel_evt=TrepereTemporel_CreerEvt; // retroCompatibilite

TrepereTemporel_evt.prototype={
	//creerCSS:function(){}

	recalcul:function(config)
		{
		var conf=(typeof(config)=='object')?config:{};
		
		// ---- recalcul des vars internes ----
		this.tps_ut_deb=this.evtData.date_deb.dt2Unite();
		this.tps_ut_fin=this.evtData.date_fin.dt2Unite();
		this.tps_ut_dur=Math.abs(this.tps_ut_fin-this.tps_ut_deb);
		//decalage par rapport a l'origine de la bande
		this.left=Math.round((this.tps_ut_deb*this.idJS_parent.ratio)-this.idJS_parent.left);
		// longueur
		var l=Math.round(this.tps_ut_dur*this.idJS_parent.ratio);	//longueur:width
		this.longueur=(l<1)?1:l;				// ne peut etre inferieur a 1 pixel

		// ---- css: application des modificateurs personalises ---- //
		
		// ==== on redimentionne === //=
		this.idCSS.style.left =this.left+'px';
		switch(this.idJS_parent.genre)
			{
			case'regle':
				this.idCSS.style.width='auto';
				this.idCSS.style.minWidth='1px';
				this.idCSS.style.maxWidth='none';
				break;
			//case'evt':
			default:
				this.idCSS.style.width=this.idCSS.style.minWidth=this.idCSS.style.maxWidth=this.longueur+'px';
				break;
			}			

		// == appelle des callbaks == //
		var cb=(typeof(conf.callbacks)=='object')?conf.callbacks:{};
		for(var cbDest in cb)//scanner chaque destination['title','texte',etc]
			{
			var f=this.callbacks[cbDest][cb[cbDest]];
			if(typeof(f)=='function')
				f(this);
			}
		return this;
		}
	/* ************************************ */
	// affiche/cache et recalcul
	// afficher
	// VISIBLE: 0:cache le bloc; SINON: l'affiche
	// recalculer:	 donnee: force le recalcul
	/* ************************************ */
	,afficher:function(visible,reclaculer){
		if(visible===undefined)visible=1;		// par defaut on affiche
		if(reclaculer!=undefined)this.recalcul({});		// si le ratio est donne alors recalcul
		this.idCSS.style.display='display:'+(visible)?'inline':'none';
		return this;
		}

	,setAttrCSS:function(conf){
		if(typeof(conf)!=='object')return null;

		// on applique les parametres donner
		if(conf.backgroundColor)this.idCSS.style.backgroundColor=conf.backgroundColor;
		if(conf.color)this.idCSS.style.color=conf.color;
		if(conf.position)this.idCSS.style.position=conf.position;
		if(conf.texte){this.texte=conf.texte;this.idCSS.innerHTML=this.texte;}
		if(conf.title)this.idCSS.title=conf.title;
		if(typeof(conf.click=='function'))
			{
			this.click=conf.click;
			this.idCSS.addEventListener("click",this.click,false);
			}
		if(typeof(conf.dblclick=='function'))
			{
			this.dblclick=conf.dblclick;
			this.idCSS.addEventListener("dblclick",this.dblclick,false);
			}

		var recalc=null;
		if(conf.left){this.idCSS.style.left=conf.left;recalc=1;}
		if(conf.top){this.idCSS.style.top=conf.top;recalc=1;}
		if(conf.height){this.idCSS.style.height=conf.height;recalc=1;}
		if(conf.width){this.idCSS.style.width=conf.width;recalc=1;}

		if(recalc)this.recalcul();
		return 1;
		}

	/* ************************************ */
	// Modele de callback
	/* ************************************ */

	,callbacksInit:function(){
		this.callbacks['title']['fin']=function(evt){
			var title=evt.evtData.date_deb.date_str();
			evt.setAttrCSS({'title':title});
			return title;
			}
		this.callbacks['title']['deb']=function(evt){
			var title=evt.evtData.date_deb.date_str();
			evt.setAttrCSS({'title':title});
			return title;
			}
		this.callbacks['texte']['Million']=function(evt){
			var texte=Math.round(evt.evtData.date_deb.an/ut.Million)+'Ma';
			evt.setAttrCSS({'texte':texte});
			return texte;
			}
		this.callbacks['title']['details']=function(evt){
			var title='';
			var tit='';
			var detail;
			detail=evt.evtData.date_deb;
			tit=' an:' +detail.an+', ms:'+detail.ms+', jr:'+detail.jr+', hr:'+detail.hr+', mn:'+detail.mn+', sc:'+detail.sc+', unite:'+detail.unite;
			title+='debut:{'+tit+'}';

			detail=evt.evtData.date_fin;
			tit=' an:' +detail.an+', ms:'+detail.ms+', jr:'+detail.jr+', hr:'+detail.hr+', mn:'+detail.mn+', sc:'+detail.sc+', unite:'+detail.unite;
			title+='fin:{'+tit+'}';
			
			evt.setAttrCSS({'title':title});
			return title;
			}
		} // callbacksInit

	} //TrepereTemporel_evt.prototype

if(typeof(gestLib)==='object')gestLib.end('repereTemporel');
