if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel-gestFrises',ver:'1.3.0',description:"gestion des frises",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel-gestFrises.js
version: cf release
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2014.11.01
role: gestion d'une frise dans son ensemble en fournissant des fonctions avancees
dependance:
	* gestLib(facultatif)
	* reperetemporel.js
	* repereTemporel-gestBandes.js
*/


/*******************************************************
 **** objet: gestion de frise  ****
// 
********************************************************/

global_Tfrise_msg=[];  // messages pour les fonctions anonymes

function Tfrise(initialisation)	{
	var init=initialisation?initialisation:{};

	// -- validation de la balise HTML du support de la frise -- //
	if(!init.idHTML)return null;	// a modifier en generer 1 si non defini 
	this.idHTML=init.idHTML;
	this.idCSS=document.getElementById(this.idHTML);
	if(!this.idCSS)return null;

	this.supportRegles=init.supportRegles?init.supportRegles:undefined;	// (idHTML) A FAIRE: supportRegles personalise
	this.supportDatas= init.supportDatas? init.supportDatas:undefined;	// (idHTML) A FAIRE: supportDatas personalise

	this.supportReglesIdCSS=document.getElementById(this.supportRegles);
	this.supportDatasIdCSS= document.getElementById(this.supportDatas);

	// -- ajout de la bande modele -- //
	if(typeof(init.bandeModele)!=='object')return null;
	
	// -- Creation de la  bande qui servira de modele au bandes affichees dans la frise (ce modele lui meme n'est pas affiche) -- //
	this.bandeModele=new TrepereTemporel_bandeData(init.bandeModele);

	// -- gestion des bandes -- //
	this.gestBandes= new TgestBandes(this);// ajout de la creation et gestion dynamique des bandes pour la frise actuelle (this)

	// *** fonctions non prototyper qui sont a surcharger par l'utilisateur *** //
	// -- exemple de modele de fonction de demarrage -- //
	//this.auto=function(config){var conf=config?config:{};}

	// -- redimentionner la frise selon la bande modele -- //
	this.idCSS.style.width=this.bandeModele.longueur+'px';
	if(this.supportReglesIdCSS)this.supportReglesIdCSS.style.width=this.idCSS.style.width;
	if(this.supportDatasIdCSS) this.supportDatasIdCSS.style.width =this.idCSS.style.width;


	//this.idCSS.style.height=this.bandeModele.hauteur+'px'; // egale a la somme des hauteurs des bandes

	// -- recalcul synchronisation des bandes -- //
	this.recalcul=function(config){this.gestBandes.recalcul(config);}
	return this;
	}

// *** fonctions prototyper: communs a totues les frises *** //

Tfrise.prototype={
	// == function de redimentionnement de la bandeModele == //
	addLeftMarge:function(marge)
		{
		if(isNaN(marge))return 0;
		this.bandeModele.left_marge=marge;
		this.idCSS.style.width=this.bandeModele.longueur+this.bandeModele.left_marge+'px';	//application de la marge gauche sans utiliser la fonction recalcul
		return this.bandeModele.left_marge;
		}

	,longueurInc:function(lg)
		{
		this.bandeModele.longueurInc(lg);
		this.recalcul();
		return this;
		}

	,dateAdd:function(debFin,uniteNb,uniteType)
		{
		this.bandeModele.date_Add(debFin,uniteNb,uniteType);
		this.recalcul();
		return this;
		}
	
	,translate:function(uniteNb,uniteType)
		{
		this.bandeModele.translate(uniteNb,uniteType);
		this.recalcul();
		return this;
		}
	,limite:function(deb,fin)
		{
		this.bandeModele.limite(deb,fin);
		this.recalcul();
		return this;
		}

	,show:function(){this.idCSS.style.display='block';return this.idCSS.style.display;}
	,hide:function(){this.idCSS.style.display='none';return this.idCSS.style.display;}

	// == function d'ajout dans la bande bandeRef de donnee (tableau remplit de dateTime)  ==//
	/*
	 * exemple
	,showData:function(bandeRef,donnees,evtConfig,cb)
		{
//		showActivity("ajoute un groupe d'evt",1);
//		if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}
		if(typeof(bandeRef)!=='object')return;

		var evtConf=(typeof(evtConfig)=='object')?evtConfig:{};
		evtConf.opacity=evtConf.opacity?evtConf.opacity:1;
		evtConf.classe=evtConf.classe?evtConf.classe:'';
		evtConf.zIndex=evtConf.zIndex?zIndex.opacity:1005;

		// gestion des callBacks
		if(typeof(cb)!='object')cb={};
		if(typeof(cb.deb)!='function')cb.deb=function(){};
		if(typeof(cb.fin)!='function')cb.fin=function(){};
			
		global_Tfrise_msg[this.idHTML]='ajout de de la prehistoire dans la bande '+bandeRef.idHTML;
		for(var p in donnees)
			{
			if(typeof(donnees[p])=='object')
				{
				//var out=t;
				out=p;
				var EvtNu=bandeRef.evtAdd(
					donnees[p]
					,{'classe': evtConf.classe,'opacity': evtConf.opacity,'zIndex': evtConf.zIndex}
					,cb
//					,{
//						 deb: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
//						,fin: function(t){showActivity(global_Tfrise_msg[this.idHTML],'add')}
//					 }
//					);
				// == application du style personnel a l'evt == //
				var attr={};
				attr.texte=p;
//				if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
				attr.click=function()
				 	{
//				 	alert('click sur evt:'+this.innerHTML,1);
					var d=document.getElementById('detailTitre');
					d.innerHTML=this.innerHTML;
				 	this.limiteZone(this.innerHTML);//nom de l'evt(meso,neo,etc)
				 	
				 	// creation des reperes dans la limite
				 	var deb=null;
				 	var fin=null;
				 	var repereNb=10;
				 	//this.creer_ReperesAuto(regleNom,deb,fin,repereNb,config);
				 	}
//				attr.dblclick=function()
//			 		{
//			 		alert('DOUBLE click sur evt:'+this.innerHTML,1);
//			 		}
				this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
				}
			}
		}
	*/
	// == function d'ajout dans la bande bandeRef de donnee (tableau remplit de dateTime)  ==//
	,creerReperesAuto:function(config){return this.gestBandes.creerReperesAuto(config);}
	,creerBande:function(b1,b2){return this.gestBandes.creerBande(b1,b2);}
	,addEvtInBande:function(bandeGenre,bandeNom,evtData,evtOpt,cb){return this.gestBandes.addEvtInBande(bandeGenre,bandeNom,evtData,evtOpt,cb);}

	} // TFrise.prototype()
if(typeof(gestLib)==='object')gestLib.end('repereTemporel-gestFrises');
