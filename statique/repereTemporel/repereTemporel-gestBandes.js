if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel-gestBandes',ver:'1.3.0',description:"gestion des bandes",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel-gestBandes.js
version: cf release
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2014.11.01
role: creation et gestion dynamique des bandes de type 'bandeRegle' ou 'bandeData'
dependance:
	* gestLib(facultatif)
	* reperetemporel.js

	
liste des functions:
 * TgestBandes(frise) : frise:objet TFrise
 * TgestBandes.prototype.creerBande=function(bandeConf,options): creait (statiquement ou dynamiquement) une bande et le met dans le tableau bandesRegle ou bandesData selon le type


Terminologie:
une bande est de 2 types: regles ou data (repereTemporel ne fait pas de distinction de type, c'est repereTemporel-gestBandes.js qui le fait)
Dans une bandeRegle sont ajoutes les elements de genre 'repere'
Dans une bandeData  sont ajoutes les elements de genre 'evt'

*/



/*******************************************************
 **** objet: gestion des bandes (regle ou evt) ****
 // crait de nouvelle instance de bande et les stocke dans un tableau
 // un indice tableau (identificateur) peut etre donner a charge au client a ce qu'il soit unique ds le tableau
 // (peut ecraser ou etre rejete au choix du client en cas de doublont; par defaut, rejeter)
 // si aucun indice donner, un pseudo aleatoire sera creer (a charge au client de recuperer cette indice
 // 
********************************************************/
function TgestBandes(frise){
	if(typeof(frise)!=='object')return null;
	this.frise=frise;
	this.bandesRegle=new Array();
	this.bandesData=new Array();
	//this.positions=new array();//ordre d'affichage des bandes //a faire
	return this;
	}

TgestBandes.prototype.recalcul=function(config){
	for(var bRef in this.bandesRegle) this.bandesRegle[bRef].recalcul(config);
	for(var bRef in this.bandesData) this.bandesData[bRef].recalcul(config);
	return 1;
	}

// renvoie le nom de la 1ere regle
TgestBandes.prototype.getRegleNom=function(){
	for(var n in this.bandesRegle)if(typeof(this.bandesRegle[n])!='undefined')return n;
	return '';
	}


//  -- crait une bande -- //
// l'element <div> est creer a partir de l'idHTML donnee, sinon une idHTML est creer aleatoirement (ou si idHTML='auto')
// retourne bc.nom (le nom calculer)
// surcharge: surchage certains parametres et options
// - options (dans surcharge)
// - - min: interval mini lors de la generation de l'idHTML
// - - max: interval maxi lors de la generation de l'idHTML
TgestBandes.prototype.creerBande=function(bandeOri,surcharge){
	if(typeof(bandeOri)!=='object')return null;

	// - gestion de la surcharge - //
	var bf=bandesFusion(bandeOri,surcharge);

	var bandeNu=null;
	
	if(!bf.genre)bf.genre='evt';

	// - calcul de l'idHTML - //
	if(!bf.idHTML || bf.idHTML == "auto")
		{
		var min=(!isNaN(bf.min))?bf.min:0;
		var max=(!isNaN(bf.max))?bf.max:1000000;
		var nu=min+Math.floor((max-min+1)*Math.random());
		var rt=(bf.genre=='evt')?'rt_bandeData_':'rt_bandeRegle_';
		bf.idHTML=rt+nu.toString();
		}


	// - recherche de l'id de la bandeParent - //
	// 1- bf.idHTML_parent
	// 2- this.supportDatas
	var idHTML_bandeParent=
		(document.getElementById(bf.idHTML_parent))
			?document.getElementById(bf.idHTML_parent)
			:(bf.genre=='evt')
				?document.getElementById(this.frise.supportDatas)
				:document.getElementById(this.frise.supportRegles)
				;

	// - recherche (calcul) du nom . Initialise bc.nom neccessaire pour le retrouver lors de scan tableau bande[???].bc.nom - //
	// defini par "nom" sinon par idHTML
	bf.nom=bf.nom?bf.nom:bf.idHTML;

	// - recupere l'idCSS s'il existe sinon le crée - //
	this.idCSS=document.getElementById(bf.idHTML);
	if(!this.idCSS)
		{
		this.idCSS=document.createElement('div');
		// -- inserer l'element dans le parent -- //
		this.idCSS_parent=idHTML_bandeParent;
		this.idCSS_parent.appendChild(this.idCSS); 
		}
	this.idCSS.setAttribute('id',bf.idHTML);
	// - crait la bande - //
	// (et met a jours le tableau de ref concernes des idHTML de la frise)
	var bandeDest=(bf.genre=='evt')?this.bandesData:this.bandesRegle;
	bandeDest[bf.nom]=new TrepereTemporel_creerBande(this.frise.bandeModele,bf);	// ne gere pas la surcharge (ici bc est bc surcharger par surcharge
	//si erreur 
	return bf.nom;
	}


// == supprime tous les evt d'une regle == //
TgestBandes.prototype.RegleClean=function(regleNom){
	if(typeof(this.bandesRegle[regleNom])=='undefined')return null;
	
//	this.bandesRegle[regleNom].evt=[];
//	this.bandesRegle[regleNom].evtNb=0;
	this.bandesRegle[regleNom].evtSupprAll();
	return 1;
	}


// == creer_ReperesAuto : creait automatique n reperes entre deb et fin  == //
// regleNom
// repereNb: nombre de reperes
// deb,fin: {'an':an,'ms':ms, etc}
// callBack={} ex {'title':'indexTableauDuTitleVoulu'}
TgestBandes.prototype.creerReperesAuto=function(config){
	var conf=config?config:{};
	var rn=conf.regleNom?conf.regleNom:this.getRegleNom();
	var regle=this.bandesRegle[rn];
	if(!regle)return null;

	var callbacks=typeof(config.callbacks=='object')?config.callbacks:{};

	if(conf.cleanAll)this.RegleClean(rn); 
	conf.repereNb=isNaN(conf.repereNb)?3:conf.repereNb;

	var deb=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.deb):new TrepereTemporel_dateTime(regle.bandeData.date_deb);
	var fin=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.fin):new TrepereTemporel_dateTime(regle.bandeData.date_fin);

	var uniteNb=Math.abs(fin.diff(deb));
	if(uniteNb===0)return null;
	var distance=uniteNb/conf.repereNb;

	var dtTemp=new TrepereTemporel_dateTime(deb);

	var classe=conf.classe?conf.classe:'';
		
	for(var regleNu=0;regleNu<conf.repereNb;regleNu++)
		{
		var dtEvt=new TrepereTemporel_dateTime(dtTemp);
		var d=dtEvt.date_str();
		var evtNu=regle.evtAdd(
			 {'titre': d,'texte':d,'date_deb': dtEvt,'callbacks':callbacks}
			,{'classe': classe}
			);
			dtTemp.addUnite(distance);
		}
	return this;
	}

TgestBandes.prototype.addEvtInBande=function(bandeGenre,bandeNom,evtData,evtOpt,cb){
	var bandeDest=(bandeGenre=="regle")?this.bandesRegle[bandeNom]:this.bandesData[bandeNom];
	if (!bandeDest)return null;
	var EvtNu=bandeDest.evtAdd(evtData,evtOpt,cb);
	return EvtNu;
	}

if(typeof(gestLib)==='object')gestLib.end('repereTemporel-gestBandes');
