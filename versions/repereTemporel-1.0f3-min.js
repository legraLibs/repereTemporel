if(typeof(gestLib)==="object"){gestLib.loadLib({nom:"repereTemporel-gestFrises",ver:"1.0f2",description:"gestion des frises",libType:"perso",isConsole:0,isVisible:1,HTMLId:"",url:"http://legral.fr/intersites/lib/perso/js/repereTemporel/"})}
/*!
fichier: repereTemporel-gestFrises.js
version: cf loadLib
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2013.10.20
role: gestion d'une frise dans son ensemble en fournissant des fonctions avancees
dependance:
	* aucune
	* gestLib(facultatif)
	
liste des functions:
 * TFrise : objet frise
 * TgestBandes.prototype.creerBande=function(bandeConf,options): creait (statiquement ou dynamiquement) une bande et le met dans le tableau bandesRegle ou bandesData selon le type


//////////////////////////
* correction de bug:
 - aucun
 
* Modifications/fonctions ajoutes:
 - creerReperesAuto:function(config) redirection vers this.gestBandes.creerReperesAuto(config)

* bugs restants:
 - la div bandeHorsSupport NE se positionne PAS par rapport a son parent
 - le redimentionnement de la longeur des bandes ne fonctionne plus

*/
;global_Tfrise_msg=[];function Tfrise(a){var b=a?a:{};if(!b.idHTML){return null}this.idHTML=b.idHTML;this.idCSS=document.getElementById(this.idHTML);if(!this.idCSS){return null}this.supportRegles=b.supportRegles?b.supportRegles:undefined;this.supportDatas=b.supportDatas?b.supportDatas:undefined;if(typeof(b.bandeModele)!=="object"){return null}this.bandeModele=new TrepereTemporel_bandeData(b.bandeModele);this.gestBandes=new TgestBandes(this);this.recalcul=function(c){this.gestBandes.recalcul(c)};return this}Tfrise.prototype={addLeftMarge:function(){this.bandeModele.left_marge=30;this.idCSS.style.width=this.bandeModele.longueur+this.bandeModele.left_marge+"px"},longueurInc:function(a){this.bandeModele.longueurInc(a);this.recalcul()},dateAdd:function(a,b,c){this.bandeModele.date_Add(a,b,c);this.recalcul()},translate:function(a,b){this.bandeModele.translate(a,b);this.recalcul()},limite:function(a,b){this.bandeModele.limite(a,b);this.recalcul()},show:function(){this.idCSS.style.display="block"},hide:function(){this.idCSS.style.display="none"},showData:function(c,h,d,b){if(typeof(c)!=="object"){return}var g=(typeof(d)=="object")?d:{};g.opacity=g.opacity?g.opacity:1;g.classe=g.classe?g.classe:"";g.zIndex=g.zIndex?zIndex.opacity:1005;if(typeof(b)!="object"){b={}}if(typeof(b.deb)!="function"){b.deb=function(){}}if(typeof(b.fin)!="function"){b.fin=function(){}}global_Tfrise_msg[this.idHTML]="ajout de de la prehistoire dans la bande "+c.idHTML;for(var f in h){if(typeof(h[f])=="object"){out=f;var e=c.evtAdd(h[f],{classe:g.classe,opacity:g.opacity,zIndex:g.zIndex},b);var a={};a.texte=f;a.click=function(){var k=document.getElementById("detailTitre");k.innerHTML=this.innerHTML;this.limiteZone(this.innerHTML);var i=null;var l=null;var j=10};this.gestBandes.bandesData[c.nom].evt[e].setAttrCSS(a)}}},creerReperesAuto:function(a){this.gestBandes.creerReperesAuto(a)}};if(typeof(gestLib)==="object"){gestLib.end("repereTemporel-gestFrises")}if(typeof(gestLib)==="object"){gestLib.loadLib({nom:"repereTemporel-gestBandes",ver:"1.0f2",description:"gestion des bandes",libType:"perso",isConsole:0,isVisible:1,HTMLId:"",url:"http://legral.fr/intersites/lib/perso/js/repereTemporel/"})}
/*!
fichier: repereTemporel-gestBandes.js
version: cf loadLib
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2013.10.20
role: creation et gestion dynamique des bandes de type 'bandeRegle' ou 'bandeData'
dependance:
	* aucune
	* gestLib(facultatif)
	
liste des functions:
 * TgestBandes(frise) : frise:objet TFrise
 * TgestBandes.prototype.creerBande=function(bandeConf,options): creait (statiquement ou dynamiquement) une bande et le met dans le tableau bandesRegle ou bandesData selon le type


Terminologie:
une bande est de 2 types: regles ou data (repereTemporel ne fait pas de distinction de type, c'est repereTemporel-gestBandes.js qui le fait)
Dans une bandeRegle sont ajoutes les elements de genre 'repere'
Dans une bandeData  sont ajoutes les elements de genre 'evt'

//////////////////////////
* correction de bug:
 - aucun
 
* Modifications/fonctions ajoutes:
 - creerReperesAuto: creation d'une regle a reperes automatiques sur une selection
 - getRegleNom: renvoie le nom de la 1ere regle creer

* bugs restants:
 - la div bandeHorsSupport NE se positionne PAS par rapport a son parent
 - le redimentionnement de la longeur des bandes ne fonctionne plus
*/
;function TgestBandes(a){if(typeof(a)!=="object"){return null}this.frise=a;this.bandesRegle=new Array();this.bandesData=new Array()}TgestBandes.prototype.recalcul=function(a){for(var b in this.bandesRegle){this.bandesRegle[b].recalcul(a)}for(var b in this.bandesData){this.bandesData[b].recalcul(a)}};TgestBandes.prototype.getRegleNom=function(){for(var a in this.bandesRegle){if(typeof(this.bandesRegle[a])!="undefined"){return a}}return null};TgestBandes.prototype.creerBande=function(f,k){var e=(typeof(f)==="object")?f:{};var a=e.genre?e.genre:"data";var b=(typeof(k)==="object")?k:{};var j=null;if(e.idHTML){j=e.idHTML}if(!e.idHTML||e.idHTML=="auto"){var d=(!isNaN(b.min))?b.min:0;var h=(!isNaN(b.max))?b.max:1000000;var g=d+Math.floor((h-d+1)*Math.random());var c=(a=="data")?"rt_bandeData_":"rt_bandeRegle_";e.idHTML=c+g.toString()}e.nom=e.nom?e.nom:b.nom?b.nom:e.idHTML;this.idCSS=document.getElementById(e.idHTML);if(!this.idCSS){this.idCSS=document.createElement("div");this.idCSS_parent=document.getElementById(e.idHTML_parent);this.idCSS_parent.appendChild(this.idCSS)}this.idCSS.setAttribute("id",e.idHTML);var i=(a=="data")?this.bandesData:this.bandesRegle;i[e.nom]=new TrepereTemporel_bande(this.frise.bandeModele,e);return e.nom};TgestBandes.prototype.RegleClean=function(a){if(typeof(this.bandesRegle[a])=="undefined"){return null}this.bandesRegle[a].evtSupprAll()};TgestBandes.prototype.creerReperesAuto=function(c){var l=c?c:{};var j=l.regleNom?l.regleNom:this.getRegleNom();var b=this.bandesRegle[j];if(!b){return null}var k=typeof(c.callbacks=="object")?c.callbacks:{};if(l.cleanAll){this.RegleClean(j)}l.repereNb=isNaN(l.repereNb)?3:l.repereNb;var g=(typeof(l.deb)=="object")?new TrepereTemporel_dateTime(l.deb):new TrepereTemporel_dateTime(b.bandeData.date_deb);var o=(typeof(l.deb)=="object")?new TrepereTemporel_dateTime(l.fin):new TrepereTemporel_dateTime(b.bandeData.date_fin);var f=Math.abs(o.diff(g));if(f===0){return null}var a=f/l.repereNb;var n=new TrepereTemporel_dateTime(g);var e=l.classe?l.classe:"";for(var i=0;i<l.repereNb;i++){var p=new TrepereTemporel_dateTime(n);var m=p.date_str();var h=b.evtAdd({titre:m,texte:m,date_deb:p,callbacks:k},{classe:e});n.addUnite(a)}};if(typeof(gestLib)==="object"){gestLib.end("repereTemporel-gestBandes")}if(typeof(gestLib)==="object"){gestLib.loadLib({nom:"repereTemporel",ver:"1.0f2",description:"afficheur de graphe temporel",libType:"perso",isConsole:0,isVisible:1,HTMLId:"",url:"http://legral.fr/intersites/lib/perso/js/repereTemporel/"})}
/*!
fichier: repereTemporel.js
version: cf gestLib
auteur:pascal TOLEDO
date de creation: 2011.12.22
date de modification: 2014.01.17
version 1.0f3
role: affiche une barre de repere temporel
dependance:
	* aucune
	* gestLib(facultatif)
	
liste des functions:
 * TrepereTemporel_new_dateTime(data) : creation d'une instance TrepereTemporel_dateTime(dt)
 * TrepereTemporel_dateTime(dt): gestion du temps
 * TrepereTemporel_bandeData(bande): donnee pour les bandes
 * TrepereTemporel_evtData(evtData): donnee pour les evt
 * TrepereTemporel_bande(bandeModele,bandeConf): gestion d'une bande
 * TrepereTemporel_evt(evtData,evtOpt,parent): gestion d'un evenenement

Terminologie:
une bande est de 2 genres: regles ou data (repereTemporel ne fait pas de distinction de type, c'est repereTemporel-gestBandes.js qui le fait)
Dans une bandeRegle sont  ajoute les elements de genre 'repere'
Dans une bandeData sont  ajoute les elements de genre 'datas'

* Modifications/fonctions ajoutes:
 - repereTemporel.js: TrepereTemporel_bande.evtSuppr(),TrepereTemporel_bande.evtSupprAll()
 - repereTemporel.js: date_fin n'est plus obligatoire. En cas d'absence de date_fin: date_fin=date_deb

*/
;ut={Million:1000000,Milliard:1000000000};function TrepereTemporel_new_dateTime(a){if(!a){return null}if(typeof(a.isDateTimeObj)=="function"){return a}return new TrepereTemporel_dateTime(a)}function TrepereTemporel_dateTime(a){this.an=this.ms=this.jr=this.hr=this.mn=this.sc=this.unite=null;if(a){if(!isNaN(a.an)){this.an=a.an}if(!isNaN(a.ms)){this.ms=a.ms}if(!isNaN(a.jr)){this.jr=a.jr}if(!isNaN(a.hr)){this.hr=a.hr}if(!isNaN(a.mn)){this.mn=a.mn}if(!isNaN(a.sc)){this.sc=a.sc}this.unite=(a.unite!=undefined)?a.unite:"jr"}}TrepereTemporel_dateTime.prototype={isDateTimeObj:function(){return 1},isNull:function(){return((this.an===null)&&(this.ms===null)&&(this.jr===null)&&(this.hr===null)&&(this.mn===null)&&(this.sc===null))},date_str:function(){if(this.isNull()){return null}var a="";if(this.jr){a+=this.jr+"/"}if(this.ms){a+=this.ms+"/"}if(this.an){a+=this.an+" "}if(this.hr){a+=this.hr+"h"}if(this.mn){a+=this.mn+"m"}if(this.sc){a+=this.sc+"s"}return a},dt2Unite:function(b){if(this.isNull()){return null}b=b?b:this.unite;var a=0;switch(b){case"an":if(this.an){a=this.an}break;case"jr":if(this.an){a+=this.an*365.25}if(this.ms){a+=this.ms*30.5}if(this.jr){a+=this.jr}break}return a},diff:function(b){var a=0;if(b&&!b.isNull()){a+=b.dt2Unite(this.unite)-this.dt2Unite()}else{a=null}return a},addUnite:function(a,b){if(isNaN(a)){return null}unit=b?b:this.unite;switch(unit){case"an":this.an+=a;return a;break;case"ms":this.ms+=a;return a;break;case"jr":this.jr+=a;return a;break;case"hr":this.hr+=a;return a;break;case"mn":this.mn+=a;return a;break;case"sc":this.sc+=a;return a;break}},setDT:function(b,a){if(typeof(b)!=="object"){return null}if(!b.zero){b.zero=0}this.an=b.an?b.an:b.zero?null:this.an;this.jr=b.jr?b.jr:b.zero?null:this.jr;this.ms=b.ms?b.ms:b.zero?null:this.ms;this.hr=b.hr?b.hr:b.zero?null:this.hr;this.mn=b.mn?b.mn:b.zero?null:this.mn;this.sc=b.sc?b.sc:b.zero?null:this.sc}};function TrepereTemporel_bandeData(a){if(!a){return null}this.date_deb=TrepereTemporel_new_dateTime(a.date_deb);this.date_fin=TrepereTemporel_new_dateTime(a.date_fin);if(this.date_fin.isNull()){this.date_fin=null;this.date_fin=this.date_deb}this.title=((a.title!=undefined)&&(a.title!=""))?a.title:"["+this.date_deb.date_str()+"|"+this.date_fin.date_str()+"]";this.top=(a.top!=undefined)?a.top:0;this.left_marge=(a.left_marge!=undefined)?a.left_marge:0;this.hauteur=(a.hauteur!=undefined)?a.hauteur:10;this.longueur=(a.longueur!=undefined)?a.longueur:0;this.left=0;return this}TrepereTemporel_bandeData.prototype={longueurInc:function(a){if(!a){return null}this.longueur+=a},date_Add:function(a,b,c){switch(a){case"deb":this.date_deb.addUnite(b,c);break;case"fin":this.date_fin.addUnite(b,c);break}},translate:function(a,c,b){this.date_Add("deb",a,c);this.date_Add("fin",a,c,b)},limite:function(a,b){this.date_deb.setDT(a);this.date_fin.setDT(b)}};function TrepereTemporel_creerBande(b,a){if(!b){return undefined}this.idHTML=(a!=undefined)?a.idHTML:null;this.nom=a.nom?a.nom:this.idHTML;this.idCSS=document.getElementById(this.idHTML);if(!this.idCSS){return undefined}this.idJS=this;this.genre="data";if(typeof(a)==="object"){if(a.genre){this.genre=a.genre}}this.bandeData=new TrepereTemporel_bandeData(b);if(typeof(a)==="object"){if(a.title){this.bandeData.title=a.title}if(a.top){this.bandeData.top=a.top}if(a.longueur){this.bandeData.longueur=a.longueur}if(a.hauteur){this.bandeData.hauteur=a.hauteur}}this.evtNb=0;this.evt=Array();this.recalcul();return this}TrepereTemporel_bande=TrepereTemporel_creerBande;TrepereTemporel_bande.prototype={afficher:function(b){var a=b?b:1;this.idCSS.style.display="display:"+a?"block":"none"},readHeight:function(){return((this.idCSS.style.height==="")?0:parseInt(this.idCSS.style.height,10))},recalcul:function(b){if(typeof(b)==="object"){if(b.title){this.bandeData=b.title}if(!isNaN(b.left)){this.bandeData.left_marge=b.left}if(!isNaN(b.top)){this.bandeData.top=b.top}if(!isNaN(b.hauteur)){this.bandeData.hauteur=b.hauteur}if(!isNaN(b.longueur)){this.bandeData.longueur=b.longueur}if(typeof(b.deb)==="object"){this.bandeData.date_deb=new repereTemporel_dateTime(b.deb)}if(typeof(b.fin)==="object"){this.bandeData.date_fin=new repereTemporel_dateTime(b.fin)}}this.tps_ut_deb=this.bandeData.date_deb.dt2Unite();this.tps_ut_fin=this.bandeData.date_fin.dt2Unite();this.tps_ut_dur=Math.abs(this.tps_ut_fin-this.tps_ut_deb);this.ratio=(this.bandeData.longueur/this.tps_ut_dur);this.left=Math.round(this.tps_ut_deb*this.ratio);if(this.idCSS){this.idCSS.style.position="absolute";this.idCSS.title=this.bandeData.title;this.idCSS.style.left=this.bandeData.left_marge+"px";this.idCSS.style.top=this.bandeData.top+"px";this.idCSS.style.height=this.bandeData.hauteur+"px";this.idCSS.style.width=this.idCSS.style.minWidth=this.idCSS.style.maxWidth=this.bandeData.longueur+"px"}for(var a in this.evt){this.evt[a].recalcul(b)}},evtAdd:function(b,d,a){this.cb=a?a:{};if(typeof(this.cb.deb)=="function"){this.cb.deb()}var c=this.evtNb;this.evt[c]=new TrepereTemporel_evt(b,d,{idHTML:this.idHTML,idJS:this.idJS});if(this.evt[this.evtNb]){this.evt[c].afficher(1,this.ratio);this.evtNb++}if(typeof(this.cb.fin)=="function"){this.cb.fin()}return c},evtSupprAll:function(){for(var a in this.evt){this.evtSuppr(a)}},evtSuppr:function(b){var a=this.evt[b];if(typeof(a)!="undefined"){this.idCSS.removeChild(a.idCSS);this.evt[b]=undefined;this.evtNb--}},evtAfficher:function(a,b){if(!b){b=1}this.evt[a].afficher(b)}};function TrepereTemporel_evtData(a){if(!a){return null}if(typeof(a.isEvtData)=="function"){return a}cb=typeof(a.callBack)=="object"?a.callBack:{};if(typeof(a.date_deb)!=="object"){return null}this.isEvtData=function(){return 1};this.typeOf=function(){return"TrepereTemporel_evtData"};this.date_deb=TrepereTemporel_new_dateTime(a.date_deb);if(!a.date_fin){a.date_fin=a.date_deb}this.date_fin=TrepereTemporel_new_dateTime(a.date_fin);var b=this.date_deb.diff(this.date_fin);if((!this.date_fin)||(this.date_fin.isNull())){this.date_fin=null;this.date_fin=this.date_deb}this.titre=(a.titre)?a.titre:"";var c=(b)?this.date_deb.date_str()+"|"+this.date_fin.date_str()+"(duree:"+b+")":this.date_deb.date_str();this.title=((a.title)&&(a.title!=""))?this.titre+" "+a.title:this.titre+"["+c+"]";if(a.texte){this.texte=a.texte}if(!this.texte){this.texte="&nbsp"}if(typeof(cb.title)=="function"){cb.title()}this.sourceTxt=(a.sourceTxt)?a.sourceTxt:"";this.sourceURL=(a.sourceURL)?a.sourceURL:"";return this}function TrepereTemporel_CreerEvt(a,c,b){if(typeof(a)!=="object"||a=={}||typeof(b)!=="object"){return undefined}if(typeof(a)!=="object"){return null}this.idHTML_parent=(b)?b.idHTML:null;this.idJS_parent=(b)?b.idJS:null;this.idCSS_parent=document.getElementById(this.idHTML_parent);this.idCSS=null;this.tps_ut_deb=0;this.tps_ut_fin=0;this.tps_ut_dur=0;this.left=0;this.longueur=0;this.evtData=new TrepereTemporel_evtData(a);if(typeof(c)=="object"){this.zIndex=c.zIndex?c.zIndex:1001;this.classe=c.classe?c.classe:null;this.opacity=c.opacity?c.opacity:null;this.hauteur=c.hauteur?c.hauteur:this.idJS_parent.readHeight()}if(!b){return null}this.idCSS=document.createElement("div");this.idCSS_parent.appendChild(this.idCSS);this.idCSS.style.position="absolute";this.idCSS.style.overflow="hidden";this.idCSS.style.top=0;this.idCSS.style.zIndex=this.zIndex;this.idCSS.style.height=this.hauteur+"px";this.idCSS.style.cursor="help";if(this.classe!=null){this.idCSS.className+=" "+this.classe}if(this.opacity!=null){this.idCSS.style.opacity=this.opacity}this.idCSS.innerHTML=this.evtData.texte;this.idCSS.title=this.evtData.title;this.callbacks=[];this.callbacks.title=[];this.callbacks.texte=[];this.callbacksInit();this.click=typeof(a.click)==="function"?a.click:function(){};this.recalcul(a);return this}TrepereTemporel_evt=TrepereTemporel_CreerEvt;TrepereTemporel_evt.prototype={recalcul:function(d){var c=(typeof(d)=="object")?d:{};this.tps_ut_deb=this.evtData.date_deb.dt2Unite();this.tps_ut_fin=this.evtData.date_fin.dt2Unite();this.tps_ut_dur=Math.abs(this.tps_ut_fin-this.tps_ut_deb);this.left=Math.round((this.tps_ut_deb*this.idJS_parent.ratio)-this.idJS_parent.left);var b=Math.round(this.tps_ut_dur*this.idJS_parent.ratio);this.longueur=(b<1)?1:b;this.idCSS.style.left=this.left+"px";switch(this.idJS_parent.genre){case"data":this.idCSS.style.width=this.idCSS.style.minWidth=this.idCSS.style.maxWidth=this.longueur+"px";break;case"regle":this.idCSS.style.width="auto";this.idCSS.style.minWidth="1px";this.idCSS.style.maxWidth="none";break}var a=(typeof(c.callbacks)=="object")?c.callbacks:{};for(var g in a){var e=this.callbacks[g][a[g]];if(typeof(e)=="function"){e(this)}}},afficher:function(b,a){if(b===undefined){b=1}if(a!=undefined){this.recalcul({})}this.idCSS.style.display="display:"+(b)?"inline":"none"},setAttrCSS:function(b){if(typeof(b)!=="object"){return}if(b.backgroundColor){this.idCSS.style.backgroundColor=b.backgroundColor}if(b.color){this.idCSS.style.color=b.color}if(b.position){this.idCSS.style.position=b.position}if(b.texte){this.texte=b.texte;this.idCSS.innerHTML=this.texte}if(b.title){this.idCSS.title=b.title}if(typeof(b.click=="function")){this.click=b.click;this.idCSS.addEventListener("click",this.click,false)}if(typeof(b.dblclick=="function")){this.dblclick=b.dblclick;this.idCSS.addEventListener("dblclick",this.dblclick,false)}var a=null;if(b.left){this.idCSS.style.left=b.left;a=1}if(b.top){this.idCSS.style.top=b.top;a=1}if(b.height){this.idCSS.style.height=b.height;a=1}if(b.width){this.idCSS.style.width=b.width;a=1}if(a){this.recalcul()}},callbacksInit:function(){this.callbacks.title["fin"]=function(a){var b=a.evtData.date_deb.date_str();a.setAttrCSS({title:b})};this.callbacks.title["deb"]=function(a){var b=a.evtData.date_deb.date_str();a.setAttrCSS({title:b})};this.callbacks.texte["Million"]=function(a){var b=Math.round(a.evtData.date_deb.an/ut.Million)+"Ma";a.setAttrCSS({texte:b})};this.callbacks.title["details"]=function(a){var d="";var b="";var c;c=a.evtData.date_deb;b=" an:"+c.an+", ms:"+c.ms+", jr:"+c.jr+", hr:"+c.hr+", mn:"+c.mn+", sc:"+c.sc+", unite:"+c.unite;d+="debut:{"+b+"}";c=a.evtData.date_fin;b=" an:"+c.an+", ms:"+c.ms+", jr:"+c.jr+", hr:"+c.hr+", mn:"+c.mn+", sc:"+c.sc+", unite:"+c.unite;d+="fin:{"+b+"}";a.setAttrCSS({title:d})}}};if(typeof(gestLib)==="object"){gestLib.end("repereTemporel")};