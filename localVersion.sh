#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier



function localConcat {
	

        # - concatenation des fichiers css - #
        catCSS ./styles/knacss.css
        catCSS ./styles/gestLib-0.1.css

	catCSS ./styles/html4.css
	catCSS ./styles/intersites.css

	catCSS ./styles/notes/notesLocales.css
	catCSS ./styles/menuOnglets-defaut/menuLocales.css
	catCSS ./styles/tutoriels/tutorielsLocales.css


        # - concatenation des fichiers js - #

#        catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js
	catJS ./repereTemporel-gestFrises.js
	catJS ./repereTemporel-gestBandes.js
	catJS ./repereTemporel.js



	# - cp de pages - #
	cp -R /www/0pages/_site/ ./pagesLocales/_site/

}

function localSave {

	# - renomme scripts(.min).js en repereTemporel(.min).js - #
	echo "renomme scripts(.min).js en repereTemporel(.min).js";
	cp ./locales/scripts.js ./styles/timeline.js
	cp ./locales/scripts.min.js ./styles/timeline.min.js

	# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
	versionSave ./repereTemporel js
	versionSave ./repereTemporel-gestBandes js
	versionSave ./repereTemporel-gestFrises js

	versionSave ./timeline js
	versionSave ./timeline.min  js

       if [ -f './scripts/statique.sh' ];then
		echo "$couleurINFO # - on statifie - #$couleurNORMAL";
               ./scripts/statique.sh
	else
		echo "$couleurWARN pas de script de statification$couleurNORMAL";
               fi


	}


