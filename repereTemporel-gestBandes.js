if(typeof(gestLib)==='object')gestLib.loadLib({nom:'repereTemporel-gestBandes',ver:'1.2.0',description:"gestion des bandes",libType:'perso',isConsole:0,isVisible:1,HTMLId:'',url:'http://legral.fr/intersites/lib/perso/js/repereTemporel/'});
/*!
fichier: repereTemporel-gestBandes.js
version: cf loadLib
auteur:pascal TOLEDO
date de creation: 2013.09
date de modification: 2013.10.20
role: creation et gestion dynamique des bandes de type 'bandeRegle' ou 'bandeData'
dependance:
	* aucune
	* gestLib(facultatif)
	
liste des functions:
 * TgestBandes(frise) : frise:objet TFrise
 * TgestBandes.prototype.creerBande=function(bandeConf,options): creait (statiquement ou dynamiquement) une bande et le met dans le tableau bandesRegle ou bandesData selon le type


Terminologie:
une bande est de 2 types: regles ou data (repereTemporel ne fait pas de distinction de type, c'est repereTemporel-gestBandes.js qui le fait)
Dans une bandeRegle sont ajoutes les elements de genre 'repere'
Dans une bandeData  sont ajoutes les elements de genre 'evt'

//////////////////////////
* correction de bug:
 - aucun
 
* Modifications/fonctions ajoutes:
 - creerReperesAuto: creation d'une regle a reperes automatiques sur une selection
 - getRegleNom: renvoie le nom de la 1ere regle creer

* bugs restants:
 - la div bandeHorsSupport NE se positionne PAS par rapport a son parent
 - le redimentionnement de la longeur des bandes ne fonctionne plus
*/



/*******************************************************
 **** objet: gestion des bandes (regle ou evt) ****
 // crait de nouvelle instance de bande et les stocke dans un tableau
 // un indice tableau (identificateur) peut etre donner a charge au client a ce qu'il soit unique ds le tableau
 // (peut ecraser ou etre rejete au choix du client en cas de doublont; par defaut, rejeter)
 // si aucun indice donner, un pseudo aleatoire sera creer (a charge au client de recuperer cette indice
 // 
********************************************************/
function TgestBandes(frise)
	{
	if(typeof(frise)!=='object')return null;
	this.frise=frise;
	this.bandesRegle=new Array();
	this.bandesData=new Array();
	//this.positions=new array();//ordre d'affichage des bandes //a faire
	}

TgestBandes.prototype.recalcul=function(config)
	{
	for(var bRef in this.bandesRegle) this.bandesRegle[bRef].recalcul(config);
	for(var bRef in this.bandesData) this.bandesData[bRef].recalcul(config);
	}

// renvoie le nom de la 1ere regle
TgestBandes.prototype.getRegleNom=function()
	{
	for(var n in this.bandesRegle)if(typeof(this.bandesRegle[n])!='undefined')return n;
	return null;
	}


//  -- crait une bande -- //
// l'element <div est creer a partir de l'idHTML donnee sinon une idHTML est creer aleatoirement (ou si idHTML='auto')
// retourne le nom calculer
TgestBandes.prototype.creerBande=function(bandeConf,options)
	{
	//if(typeof(frise.bandeModele)!=='object')return null;
	var bc=(typeof(bandeConf)==='object')?bandeConf:{};
	//	if(!bc.idHTML_parent)return;
	var bandeGenre=bc.genre?bc.genre:'data';//['data','regle']
	
	var o=(typeof(options)==='object')?options:{};	// surcharge (nom,...)

	var bandeNu=null;

	//on determine l'indice du tableau
	if(bc.idHTML)bandeNu=bc.idHTML;	// idHTML est donner 
	if(!bc.idHTML||bc.idHTML=='auto')	// pas preciser ou auto: creation automatique d'une idHTML
		{
		var min=(!isNaN(o.min))?o.min:0;
		var max=(!isNaN(o.max))?o.max:1000000;
		var nu=min+Math.floor((max-min+1)*Math.random());
		var rt=(bandeGenre=='data')?'rt_bandeData_':'rt_bandeRegle_';
		bc.idHTML=rt+nu.toString();
		}

	// recherche (calcul) du nom . Initialise bc.nom neccessaire pour le retrouver lors de scan tableau bande[???].bc.nom
	bc.nom=bc.nom?bc.nom:o.nom?o.nom:bc.idHTML;

	//recupere l'idCSS s'il existe sinon le crait
		this.idCSS=document.getElementById(bc.idHTML);
		if(!this.idCSS)
			{
			this.idCSS=document.createElement('div');
			// inserer l'element dans le parent
			this.idCSS_parent=document.getElementById(bc.idHTML_parent);
			this.idCSS_parent.appendChild(this.idCSS); 
			}
		this.idCSS.setAttribute('id',bc.idHTML);
	// crait la bande 
	// (et met a jours le tableau de ref concernes des idHTML de la frise)
	var bandeDest=(bandeGenre=='data')?this.bandesData:this.bandesRegle
	bandeDest[bc.nom]=new TrepereTemporel_bande(this.frise.bandeModele,bc);
	//si erreur 
	return bc.nom;
	}


// == supprime tous les evt d'une regle == //
TgestBandes.prototype.RegleClean=function(regleNom)
	{
	if(typeof(this.bandesRegle[regleNom])=='undefined')return null;
	
//	this.bandesRegle[regleNom].evt=[];
//	this.bandesRegle[regleNom].evtNb=0;
	this.bandesRegle[regleNom].evtSupprAll();

	}


// == creer_ReperesAuto : creait automatique n reperes entre deb et fin  == //
// regleNom
// repereNb: nombre de reperes
// deb,fin: {'an':an,'ms':ms, etc}
// callBack={} ex {'title':'indexTableauDuTitleVoulu'}
TgestBandes.prototype.creerReperesAuto=function(config)
		{
		var conf=config?config:{};
		var rn=conf.regleNom?conf.regleNom:this.getRegleNom();
		var regle=this.bandesRegle[rn];
		if(!regle)return null;

		var callbacks=typeof(config.callbacks=='object')?config.callbacks:{};

		if(conf.cleanAll)this.RegleClean(rn); 
		conf.repereNb=isNaN(conf.repereNb)?3:conf.repereNb;

		var deb=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.deb):new TrepereTemporel_dateTime(regle.bandeData.date_deb);
		var fin=(typeof(conf.deb)=='object')?new TrepereTemporel_dateTime(conf.fin):new TrepereTemporel_dateTime(regle.bandeData.date_fin);

		var uniteNb=Math.abs(fin.diff(deb));
		if(uniteNb===0)return null;
		var distance=uniteNb/conf.repereNb;

		var dtTemp=new TrepereTemporel_dateTime(deb);

		var classe=conf.classe?conf.classe:'';
		
		for(var regleNu=0;regleNu<conf.repereNb;regleNu++)
			{
			var dtEvt=new TrepereTemporel_dateTime(dtTemp);
			var d=dtEvt.date_str();
			var evtNu=regle.evtAdd(
				 {'titre': d,'texte':d,'date_deb': dtEvt,'callbacks':callbacks}
				,{'classe': classe}
				);

			dtTemp.addUnite(distance);
			}

		}

if(typeof(gestLib)==='object')gestLib.end('repereTemporel-gestBandes');
